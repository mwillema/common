package com.marcowillemart.common.projection;

import com.marcowillemart.common.CommonProjectionConfig;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Projection auto configuration.
 *
 * @author Marco Willemart
 */
@Configuration
@ConditionalOnClass(CommonProjectionConfig.class)
@Import(CommonProjectionConfig.class)
public class ProjectionAutoConfiguration {

    /**
     * @effects Makes this be a new projection auto configuration.
     */
    public ProjectionAutoConfiguration() {
    }
}
