package com.marcowillemart.common.web;

import com.marcowillemart.common.util.MessageTypeRegistry;
import java.util.function.Supplier;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.cache.CacheConfig;
import org.apache.http.impl.client.cache.CachingHttpClients;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

/**
 * Web auto configuration.
 *
 * @author Marco Willemart
 */
@Configuration
@ComponentScan
@ConditionalOnClass(GlobalExceptionHandler.class)
public class WebAutoConfiguration {

    /**
     * @effects Makes this be a new web auto configuration.
     */
    public WebAutoConfiguration() {
    }

    /**
     * @return the Message type registry
     */
    @Bean
    public MessageTypeRegistry messageTypeRegistry() {
        return new MessageTypeRegistry();
    }

    /**
     * @requires registry != null
     * @return the Protocol Buffers HTTP message converter supporting the
     *         application/x-protobuf and application/json media types and able
     *         to convert the Any types registered in the given registry
     */
    @Bean
    public ProtobufHttpMessageConverter protobufHttpMessageConverter(
            MessageTypeRegistry registry) {

        return new ProtobufHttpMessageConverter(registry);
    }

    /**
     * @return the supplier of request factories using a caching HTTP client.
     */
    @Bean
    public Supplier<ClientHttpRequestFactory> httpRequestFactorySupplier() {
        return () -> {
            CacheConfig cacheConfig = CacheConfig.custom()
                    .setSharedCache(false)
                    .build();

            HttpClient cachingClient = CachingHttpClients.custom()
                    .setCacheConfig(cacheConfig)
                    .build();

            return new HttpComponentsClientHttpRequestFactory(cachingClient);
        };
    }
}
