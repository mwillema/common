package com.marcowillemart.common.web;

import com.marcowillemart.common.util.MessageTypeRegistry;
import java.util.function.Supplier;
import static org.assertj.core.api.Assertions.*;
import org.junit.Test;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;
import org.springframework.http.client.ClientHttpRequestFactory;

/**
 * Unit tests for the WebAutoConfiguration class.
 *
 * @author Marco Willemart
 */
public class WebAutoConfigurationTest {

    private final ApplicationContextRunner contextRunner =
            new ApplicationContextRunner()
                    .withConfiguration(
                            AutoConfigurations.of(WebAutoConfiguration.class));

    @Test
    public void testDefaultAutoConfiguration() {
        contextRunner.run(context -> {
            assertThat(context)
                    .hasSingleBean(MessageTypeRegistry.class);

            assertThat(context)
                    .hasSingleBean(ProtobufHttpMessageConverter.class);

            assertThat(context).hasSingleBean(Supplier.class);

            Supplier<?> supplier = context.getBean(Supplier.class);

            assertThat(context.getBean(Supplier.class).get())
                    .isInstanceOf(ClientHttpRequestFactory.class);

            assertThat(context).hasSingleBean(GlobalExceptionHandler.class);
        });
    }
}
