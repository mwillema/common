package com.marcowillemart.common.web;

import com.google.protobuf.Message;
import com.google.protobuf.util.JsonFormat;
import com.marcowillemart.common.util.Assert;
import com.marcowillemart.common.util.MessageTypeRegistry;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

/**
 * ProtobufHttpMessageConverter is an HTTP message converter that reads and
 * writes Messages using Google Protocol Buffers.
 *
 * By default, it supports the "application/x-protobuf" and "application/json"
 * media types.
 *
 * Requires at least Protobuf 3.0.0.
 *
 * @author Marco Willemart
 */
public class ProtobufHttpMessageConverter
        extends AbstractHttpMessageConverter<Message> {

    /** Header containing the name of the Protobuf schema. */
    public static final String X_PROTOBUF_SCHEMA_HEADER = "X-Protobuf-Schema";

    /** X-Protobuf-Message header. */
    public static final String X_PROTOBUF_MESSAGE_HEADER = "X-Protobuf-Message";

    private static final String METHOD_NAME = "newBuilder";

    private static final ConcurrentMap<Class<? extends Message>, Method>
            METHOD_CACHE = new ConcurrentHashMap<>();

    private final MessageTypeRegistry registry;

    /**
     * @requires registry != null
     * @effects Makes this be a new ProtobufHttpMessageConverter supporting the
     *         application/x-protobuf and application/json media types and able
     *         to convert the Any types registered in the given registry
     */
    public ProtobufHttpMessageConverter(MessageTypeRegistry registry) {
        super(MediaTypes.PROTOBUF, MediaTypes.JSON);

        Assert.notNull(registry);

        this.registry = registry;
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return Message.class.isAssignableFrom(clazz);
    }

    @Override
    protected MediaType getDefaultContentType(Message message) {
        return MediaTypes.PROTOBUF;
    }

    @Override
    protected Message readInternal(
            Class<? extends Message> clazz,
            HttpInputMessage inputMessage)
            throws IOException, HttpMessageNotReadableException {

        MediaType contentType = contentTypeFrom(inputMessage.getHeaders());

        Charset charset = charsetFrom(contentType);

        Message.Builder builder = messageBuilderFrom(clazz);

        if (MediaTypes.PROTOBUF.isCompatibleWith(contentType)) {
            builder.mergeFrom(inputMessage.getBody());
        } else if (MediaTypes.JSON.isCompatibleWith(contentType)) {
            Reader reader =
                    new InputStreamReader(inputMessage.getBody(), charset);
            JsonFormat.parser()
                    .usingTypeRegistry(registry.typeRegistry())
                    .merge(reader, builder);
        } else {
            Assert.shouldNeverGetHere();
        }

        return builder.build();
    }

    @Override
    protected void writeInternal(
            Message message,
            HttpOutputMessage outputMessage)
            throws IOException, HttpMessageNotWritableException {

        MediaType contentType = contentTypeFrom(outputMessage.getHeaders());

        Charset charset = charsetFrom(contentType);

        if (MediaTypes.PROTOBUF.isCompatibleWith(contentType)) {
            // Note: getHeaders() must be called before getBody() because the
            //       later writes HTTP headers (making them read only).
            setProtobufHeaders(outputMessage.getHeaders(), message);
            message.writeTo(outputMessage.getBody());
        } else if (MediaTypes.JSON.isCompatibleWith(contentType)) {
            Writer writer =
                    new OutputStreamWriter(outputMessage.getBody(), charset);
            writer.write(
                    JsonFormat.printer()
                            .usingTypeRegistry(registry.typeRegistry())
                            .print(message));
            writer.flush();
        } else {
            Assert.shouldNeverGetHere();
        }
    }

    /**
     * @requires headers != null
     * @return the content type from the given headers
     */
    private static MediaType contentTypeFrom(HttpHeaders headers) {
        MediaType contentType = headers.getContentType();

        if (contentType == null) {
            contentType = MediaTypes.PROTOBUF;
        }

        return contentType;
    }

    /**
     * @requires contentType != null
     * @return the charset from the given content type
     */
    private static Charset charsetFrom(MediaType contentType) {
        Charset charset = contentType.getCharset();

        if (charset == null) {
            charset = MediaTypes.DEFAULT_CHARSET;
        }

        return charset;
    }

    /**
     * @requires headers != null && message != null
     * @modifies headers
     * @effects Set the "X-Protobuf-*" HTTP headers when responding with a
     *          message of content type "application/x-protobuf".
     */
    private static void setProtobufHeaders(
            HttpHeaders headers,
            Message message) {

        headers.set(
                X_PROTOBUF_SCHEMA_HEADER,
                message.getDescriptorForType().getFile().getName());

        headers.set(
                X_PROTOBUF_MESSAGE_HEADER,
                message.getDescriptorForType().getFullName());
    }

    /**
     * @requires messageType != null
     * @return a new Message.Builder instance for the given Message type.
     * @throws HttpMessageNotReadableException if a Message.Builder instance
     *         cannot be created for whatever reason.
     */
    private static Message.Builder messageBuilderFrom(
            Class<? extends Message> messageType)
            throws HttpMessageNotReadableException {

        if (!METHOD_CACHE.containsKey(messageType)) {
            cacheNewBuilderMethodFor(messageType);
        }

        Method method = METHOD_CACHE.get(messageType);

        try {
            return (Message.Builder) method.invoke(messageType);
        } catch (IllegalAccessException
                | IllegalArgumentException
                | InvocationTargetException ex) {
            throw new HttpMessageNotReadableException(
                    String.format(
                            "Method cannot be invoked: %s.%s()",
                            messageType.getSimpleName(),
                            METHOD_NAME),
                    ex);
        }
    }

    /**
     * @requires aggregateType != null
     * @effects Puts a new entry <k,v> into the cache of newBuilder methods
     *          where k = messageType and v is the newBuilder method of Message
     *          'messageType'
     * @throws HttpMessageNotReadableException if a new entry cannot be put into
     *         the cache for whatever reason.
     */
    private static void cacheNewBuilderMethodFor(
            Class<? extends Message> messageType)
            throws HttpMessageNotReadableException {

        try {
            Method method = messageType.getMethod(METHOD_NAME);

            METHOD_CACHE.put(messageType, method);
        } catch (NoSuchMethodException ex) {
            throw new HttpMessageNotReadableException(
                    String.format(
                            "No such method: %s.%s()",
                            messageType.getSimpleName(),
                            METHOD_NAME),
                    ex);
        }
    }
}
