package com.marcowillemart.common.web;

import java.nio.charset.Charset;
import org.springframework.http.MediaType;

/**
 * MediaTypes is a utility class that provides commonly used media types.
 *
 * @author Marco Willemart
 */
public final class MediaTypes {

    /** Default charset, i.e., UTF-8. */
    public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

    /** Media type for application/json;charset=UTF-8. */
    public static final MediaType JSON =
            new MediaType("application", "json", DEFAULT_CHARSET);

    /** Media type for application/x-protobuf;charset=UTF-8. */
    public static final MediaType PROTOBUF =
            new MediaType("application", "x-protobuf", DEFAULT_CHARSET);

    /** this cannot be instantiated */
    private MediaTypes() {
        throw new AssertionError();
    }
}
