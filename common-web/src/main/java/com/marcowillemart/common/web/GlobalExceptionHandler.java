package com.marcowillemart.common.web;

import com.marcowillemart.common.util.NotPossibleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * GlobalExceptionHandler handles various exceptions and converts them to
 * appropriate error responses with a Date header set to the time of the error.
 *
 * @author Marco Willemart
 */
@ControllerAdvice
public final class GlobalExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(
            GlobalExceptionHandler.class);

    /**
     * @effects Makes this be a new global exception handler.
     */
    public GlobalExceptionHandler() {
    }

    /**
     * @requires ex != null && ex instanceof NotPossibleException
     * @return a new error response with status code 400 for the exception ex.
     */
    @ExceptionHandler(NotPossibleException.class)
    public ResponseEntity<String> handleNotPossibleException(Exception ex) {
        return errorResponse(HttpStatus.BAD_REQUEST, ex);
    }

    /**
     * @requires ex != null && ex instanceof RuntimeException
     * @return a new error response with status code 500 for the exception ex.
     */
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> handleRuntimeException(Exception ex) {
        LOG.error(ex.getMessage(), ex);
        return errorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex);
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    /**
     * @requires status != null && ex != null
     * @return a new error response with the HTTP status 'status' and for the
     *         exception 'ex'.
     */
    private static ResponseEntity<String> errorResponse(HttpStatus status,
            Exception ex) {
        return new ResponseEntity<>(ex.getMessage(), status);
    }
}
