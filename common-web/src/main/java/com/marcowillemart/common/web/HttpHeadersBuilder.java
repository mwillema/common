package com.marcowillemart.common.web;

import com.marcowillemart.common.util.Assert;
import com.marcowillemart.common.util.Builder;
import com.marcowillemart.common.util.Pair;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * HttpHeadersBuilder represents a mutable builder for HTTP headers.
 *
 * @author Marco Willemart
 */
public final class HttpHeadersBuilder implements Builder<HttpHeaders> {

    private final List<Pair<String, String>> headers;

    /**
     * @effects Makes this be a new HTTP headers builder.
     */
    public HttpHeadersBuilder() {
        this.headers = new LinkedList<>();
    }

    /**
     * @requires mediaType != null
     * @modifies this
     * @effects Adds an ccept header for the given media type to this
     * @return this
     */
    public HttpHeadersBuilder accept(MediaType mediaType) {
        Assert.notNull(mediaType);

        headers.add(Pair.of(HttpHeaders.ACCEPT, mediaType.toString()));

        return this;
    }

    /**
     * @requires mediaType != null
     * @modifies this
     * @effects Adds a Content-Type header for the given media type to this
     * @return this
     */
    public HttpHeadersBuilder contentType(MediaType mediaType) {
        Assert.notNull(mediaType);

        headers.add(Pair.of(HttpHeaders.CONTENT_TYPE, mediaType.toString()));

        return this;
    }

    /**
     * @requires username != null && password != null
     * @modifies this
     * @effects Adds a basic authorization header with the given username and
     *          password to this.
     * @return this
     */
    public HttpHeadersBuilder basicAuthorization(String username,
            String password) {
        Assert.notNull(username);
        Assert.notNull(password);

        String auth = String.format("%s:%s", username, password);
        byte[] encodedAuth =
                Base64.encodeBase64(auth.getBytes(Charset.defaultCharset()));
        String authHeader =
                "Basic " + new String(encodedAuth, Charset.defaultCharset());

        headers.add(Pair.of(HttpHeaders.AUTHORIZATION, authHeader));

        return this;
    }

    @Override
    public HttpHeaders build() {
        HttpHeaders result = new HttpHeaders();

        for (Pair<String, String> p : headers) {
            result.add(p.left(), p.right());
        }

        return result;
    }
}
