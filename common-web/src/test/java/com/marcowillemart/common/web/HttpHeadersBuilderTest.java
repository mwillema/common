package com.marcowillemart.common.web;

import java.util.Arrays;
import java.util.List;
import org.apache.commons.codec.binary.Base64;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Unit tests for the HttpHeadersBuilder class.
 *
 * @author Marco Willemart
 */
public class HttpHeadersBuilderTest {

    private HttpHeadersBuilder target;

    @Before
    public void setUp() {
        target = new HttpHeadersBuilder();
    }

    @Test
    public void testAccept() {
        // Setup
        final MediaType mediaType = MediaType.TEXT_HTML;

        // Exercise
        target.accept(mediaType);

        // Verify
        List<MediaType> expected = Arrays.asList(mediaType);
        List<MediaType> actual = target.build().getAccept();
        assertEquals(expected, actual);
    }

    @Test
    public void testAccept_multiple() {
        // Setup
        final MediaType mediaType1 = MediaType.IMAGE_JPEG;
        final MediaType mediaType2 = MediaType.APPLICATION_PDF;
        target.accept(mediaType1);

        // Exercise
        target.accept(mediaType2);

        // Verify
        List<MediaType> expected = Arrays.asList(mediaType1, mediaType2);
        List<MediaType> actual = target.build().getAccept();
        assertEquals(expected, actual);
    }

    @Test
    public void testContentType() {
        // Setup
        final MediaType mediaType = MediaTypes.JSON;

        // Exercise
        target.contentType(mediaType);

        // Verify
        assertEquals(MediaTypes.JSON, target.build().getContentType());
    }

    @Test
    public void testBasicAuthorization() {
        // Setup
        final String username = "guest";
        final String password = "azerty";

        // Exercise
        target.basicAuthorization(username, password);

        // Verify
        String expected = "Basic " + new String(Base64.encodeBase64(
                (username + ":" + password).getBytes()));
        String actual = target.build().getFirst(HttpHeaders.AUTHORIZATION);
        assertEquals(expected, actual);
    }

    @Test
    public void testBuild() {
        // Exercise
        HttpHeaders actual = target.build();

        // Verify
        assertTrue(actual.isEmpty());
    }
}
