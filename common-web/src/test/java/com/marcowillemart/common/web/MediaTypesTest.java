package com.marcowillemart.common.web;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests for the MediaTypes class.
 *
 * @author Marco Willemart
 */
public class MediaTypesTest {

    @Test
    public void testJson() {
        assertEquals("application/json;charset=UTF-8",
                MediaTypes.JSON.toString());
    }

    @Test
    public void testProtobuf() {
        assertEquals("application/x-protobuf;charset=UTF-8",
                MediaTypes.PROTOBUF.toString());
    }
}
