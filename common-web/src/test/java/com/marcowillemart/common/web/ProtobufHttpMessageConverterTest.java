package com.marcowillemart.common.web;

import com.marcowillemart.common.util.MessageTypeRegistry;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;

/**
 * Unit tests for the ProtobufHttpMessageConverter.
 *
 * @author Marco Willemart
 */
public class ProtobufHttpMessageConverterTest {

    private ProtobufHttpMessageConverter target;

    @Before
    public void setUp() {
        target =
                new ProtobufHttpMessageConverter(
                        new MessageTypeRegistry());
    }

    @Test
    public void testGetSupportedMediaTypes() {
        // Setup
        List<MediaType> expected =
                Arrays.asList(MediaTypes.PROTOBUF, MediaTypes.JSON);

        // Exercise
        List<MediaType> actual = target.getSupportedMediaTypes();

        // Verify
        assertEquals(expected, actual);
    }
}
