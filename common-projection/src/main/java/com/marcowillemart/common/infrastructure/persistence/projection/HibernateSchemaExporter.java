package com.marcowillemart.common.infrastructure.persistence.projection;

import com.marcowillemart.common.projection.SchemaExporter;
import java.util.EnumSet;
import org.hibernate.HibernateException;
import org.hibernate.boot.Metadata;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaValidator;
import org.hibernate.tool.schema.TargetType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hibernate implementation of the SchemaExporter interface.
 *
 * @author Marco Willemart
 */
public class HibernateSchemaExporter implements SchemaExporter {

    private static final Logger LOG =
            LoggerFactory.getLogger(HibernateSchemaExporter.class);

    private final Metadata metadata;
    private final ServiceRegistry serviceRegistry;

    /**
     * @effects Makes this be a new Hibernate schema exporter.
     */
    public HibernateSchemaExporter() {
        this.metadata = HibernateUtils.metadata();
        this.serviceRegistry = HibernateUtils.serviceRegistry();
    }

    @Override
    public boolean isSchemaValid() {
        try {
            new SchemaValidator().validate(metadata, serviceRegistry);
            LOG.info("Valid schema");
            return true;
        } catch (HibernateException ex) {
            LOG.info(ex.getMessage());
            return false;
        }
    }

    @Override
    public void export() {
        new SchemaExport()
                .create(
                        EnumSet.of(TargetType.DATABASE, TargetType.STDOUT),
                        metadata);
        LOG.info("Schema exported");
    }
}
