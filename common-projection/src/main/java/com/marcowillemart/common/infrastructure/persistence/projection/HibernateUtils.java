package com.marcowillemart.common.infrastructure.persistence.projection;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.service.ServiceRegistry;
import org.springframework.util.Assert;

final class HibernateUtils {

    private static Metadata metadata;
    private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;

    private HibernateUtils() {
        throw new AssertionError();
    }

    static Metadata metadata() {
        Assert.notNull(metadata, "Metadata should not be null");

        return metadata;
    }

    static SessionFactory sessionFactory() {
        Assert.notNull(sessionFactory, "SessionFactory should not be null");

        return sessionFactory;
    }

    static ServiceRegistry serviceRegistry() {
        Assert.notNull(serviceRegistry, "ServiceRegistry should not be null");

        return serviceRegistry;
    }

    static void initMetadata(Metadata metadata) {
        Assert.isNull(HibernateUtils.metadata, "Metadata already initialized");
        Assert.notNull(metadata, "Metadata should not be null");

        HibernateUtils.metadata = metadata;
    }

    static void initSessionFactory(SessionFactory sessionFactory) {
        Assert.isNull(HibernateUtils.sessionFactory,
                "SessionFactory already initialized");
        Assert.notNull(sessionFactory, "SessionFactory should not be null");

        HibernateUtils.sessionFactory = sessionFactory;
    }

    static void initServiceRegistry(ServiceRegistry serviceRegistry) {
        Assert.isNull(HibernateUtils.serviceRegistry,
                "ServiceRegistry already initialized");
        Assert.notNull(serviceRegistry, "ServiceRegistry should not be null");

        HibernateUtils.serviceRegistry = serviceRegistry;
    }
}
