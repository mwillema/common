package com.marcowillemart.common.infrastructure.persistence.projection;

import org.hibernate.boot.Metadata;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.integrator.spi.Integrator;
import org.hibernate.service.spi.SessionFactoryServiceRegistry;

/**
 * Utility class called during session factory initialization in order to be
 * able to integrate with Hibernate.
 *
 * @author Marco Willemart
 */
public final class HibernateIntegrator implements Integrator {

    /**
     * @effects Makes this be a new Hibernate integrator.
     */
    public HibernateIntegrator() {
    }

    @Override
    public void integrate(
            Metadata metadata,
            SessionFactoryImplementor sessionFactory,
            SessionFactoryServiceRegistry serviceRegistry) {

        HibernateUtils.initMetadata(metadata);
        HibernateUtils.initSessionFactory(sessionFactory);
        HibernateUtils.initServiceRegistry(serviceRegistry);
    }

    @Override
    public void disintegrate(
            SessionFactoryImplementor sessionFactory,
            SessionFactoryServiceRegistry serviceRegistry) {
    }
}
