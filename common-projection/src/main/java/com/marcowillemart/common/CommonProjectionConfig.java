package com.marcowillemart.common;

import com.marcowillemart.common.infrastructure.persistence.projection.HibernateSchemaExporter;
import com.marcowillemart.common.projection.ProjectionManager;
import com.marcowillemart.common.projection.SchemaExporter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Common Projection configuration.
 *
 * @author Marco Willemart
 */
@Configuration
@ComponentScan(basePackageClasses = ProjectionManager.class)
public class CommonProjectionConfig {

    /**
     * @effects Makes this be a new common projection configuration.
     */
    public CommonProjectionConfig() {
    }

    /**
     * @return the schema exporter
     */
    @Bean
    public SchemaExporter schemaExporter() {
        return new HibernateSchemaExporter();
    }
}
