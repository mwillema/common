package com.marcowillemart.common.projection;

import com.marcowillemart.common.util.Assert;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * ProjectionManager is responsible for managing the projections of an
 * application.
 *
 * @author Marco Willemart
 */
@Component
public class ProjectionManager {

    private static final Logger LOG =
            LoggerFactory.getLogger(ProjectionManager.class);

    private final SchemaExporter schemaExporter;
    private final ProjectionDispatcher projectionDispatcher;

    private final AtomicBoolean started;

    /**
     * @requires all dependencies are not null
     * @effects Makes this be a new ProjectionManager with the given
     *          dependencies.
     */
    public ProjectionManager(
            SchemaExporter schemaExporter,
            ProjectionDispatcher projectionDispatcher) {

        Assert.notNull(schemaExporter);
        Assert.notNull(projectionDispatcher);

        this.schemaExporter = schemaExporter;
        this.projectionDispatcher = projectionDispatcher;

        this.started = new AtomicBoolean(false);
    }

    /**
     * @effects Starts the projections of the application
     */
    @PostConstruct
    public void startProjections() {
        LOG.info("Starting projections...");

        Assert.isFalse(started.getAndSet(true));

        try {
            if (schemaExporter.isSchemaValid()) {
                projectionDispatcher.resume();
                LOG.info("Projections resumed");
            } else {
                schemaExporter.export();
                projectionDispatcher.restart();
                LOG.info("Projections restarted");
            }
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw ex;
        }
    }
}
