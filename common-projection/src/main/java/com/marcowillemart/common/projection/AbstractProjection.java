package com.marcowillemart.common.projection;

import com.google.protobuf.Message;
import com.marcowillemart.common.util.Assert;
import com.marcowillemart.common.util.FailureException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * AbstractProjection is an abstract implementation of the Projection interface
 * using Spring Data CRUD repositories.
 *
 * @author Marco Willemart
 */
public abstract class AbstractProjection<T> implements Projection {

    private static final Logger LOG =
            LoggerFactory.getLogger(AbstractProjection.class);

    private static final String PROJECTION_METHOD_NAME = "when";

    private static final Map<String, Method> PROJECTION_METHODS =
            new HashMap<>();

    private final Set<Class<?>> understoodEventTypes;

    @Autowired
    private CrudRepository<T, String> repository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    /**
     * @effects Makes this be a new abstract projection
     */
    protected AbstractProjection() {
        this.understoodEventTypes = new HashSet<>();

        initUnderstoodEventTypes();
    }

    @Override
    public final void dispatch(Message event) {
        Assert.notNull(event);

        if (understands(event)) {
            LOG.trace("Dispatching: {}", event.getClass().getSimpleName());
            projectWhen(event);
        }
    }

    /**
     * @requires data != null
     * @modifies this
     * @effects Saves the given piece of data in this
     */
    protected void save(T data) {
        Assert.notNull(data);

        repository.save(data);
    }

    /**
     * @requires id != null && there exists a piece of data identified by id
     * @return the piece of data represented by id
     */
    protected T find(String id) {
        Assert.notNull(id);

        return repository.findById(id).orElseThrow(FailureException::new);
    }

    /**
     * @requires id != null && execute != null &&
     *           there exists a piece of data identified by id
     * @modifies this
     * @effects Updates the piece of data in this with the given consumer
     */
    protected void update(String id, Consumer<T> execute) {
        transactionTemplate.execute(status -> {
            T data = find(id);
            execute.accept(data);
            save(data);
            return data;
        });
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    /**
     * @requires the understood even types of this have not been initialized yet
     * @modifies this
     * @effects Initializes the understood event types of this
     */
    private void initUnderstoodEventTypes() {
        Method[] methods = getClass().getDeclaredMethods();

        for (Method method : methods) {
            if (PROJECTION_METHOD_NAME.equals(method.getName())) {
                Class<?>[] parameterTypes = method.getParameterTypes();

                if (parameterTypes.length > 0) {
                    understoodEventTypes.add(parameterTypes[0]);
                }
            }
        }
    }

    /**
     * @requires event != null
     * @return true iff the given event is understood by this, i.e., can be
     *         projected on this
     */
    private boolean understands(Message event) {
        return understoodEventTypes.contains(event.getClass());
    }

    /**
     * @requires event != null
     * @modifies this
     * @effects Anything, i.e., projects the given event on this
     */
    private void projectWhen(Message event) {
        Class<? extends Projection> rootType = getClass();
        Class<? extends Message> eventType = event.getClass();

        String key =
                String.format("%s:%s", rootType.getName(), eventType.getName());

        if (!PROJECTION_METHODS.containsKey(key)) {
            cacheMutatorMethodFor(key, rootType, eventType);
        }

        Method mutatorMethod = PROJECTION_METHODS.get(key);

        try {
            mutatorMethod.invoke(this, event);
        } catch (ReflectiveOperationException | IllegalArgumentException ex) {
            throw new FailureException(
                    String.format(
                            "Method cannot be invoked: %s.%s(%s)",
                            rootType.getSimpleName(),
                            PROJECTION_METHOD_NAME,
                            eventType.getSimpleName()),
                    ex);
        }
    }

    /**
     * @requires key != null && rootType != null && eventType != null
     * @modifies this
     * @effects Puts a new entry <k,v> into the cache of projection methods
     *          where k = key and v is the projection method of root type
     *          'rootType' for the event 'eventType'.
     */
    private static void cacheMutatorMethodFor(
            String key,
            Class<? extends Projection> rootType,
            Class<? extends Message> eventType) {

        try {
            Method method =
                    rootType.getDeclaredMethod(
                            PROJECTION_METHOD_NAME,
                            eventType);

            method.setAccessible(true);

            PROJECTION_METHODS.put(key, method);
        } catch (NoSuchMethodException ex) {
            throw new FailureException(
                    String.format(
                            "No such method: %s.%s(%s)",
                            rootType.getSimpleName(),
                            PROJECTION_METHOD_NAME,
                            eventType.getSimpleName()),
                    ex);
        }
    }
}
