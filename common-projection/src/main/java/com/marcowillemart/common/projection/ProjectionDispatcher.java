package com.marcowillemart.common.projection;

import com.google.protobuf.Message;
import com.marcowillemart.common.event.EventStore;
import com.marcowillemart.common.util.Assert;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 * ProjectionDispatcher is responsible for dispatching the newly appended events
 * to the projections of the application.
 *
 * @author Marco Willemart
 */
@Component
public class ProjectionDispatcher {

    private final List<Projection> registeredProjections;
    private final EventStore eventStore;

    /**
     * @requires all dependencies are not null
     * @effects Makes this be a new projection dispatcher with the given
     *          dependencies.
     */
    public ProjectionDispatcher(
            List<Projection> registeredProjections,
            EventStore eventStore) {

        Assert.notNull(registeredProjections);
        Assert.notNull(eventStore);

        this.registeredProjections = registeredProjections;
        this.eventStore = eventStore;
    }

    /**
     * @effects Resumes the dispatching of events, i.e., dispatches the newly
     *          appended events
     */
    public void resume() {
        subscribeToAll(eventStore.lastStoredEventId());
    }

    /**
     * @effects Restarts the dispatching of events, i.e., dispatches all the
     *          events as well as the newly appended ones.
     */
    public void restart() {
        subscribeToAll(0L);
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private void subscribeToAll(long afterEventId) {
        eventStore.subscribeToAll(afterEventId, this::dispatch);
    }

    private void dispatch(Message event) {
        registeredProjections.forEach(projection -> projection.dispatch(event));
    }
}
