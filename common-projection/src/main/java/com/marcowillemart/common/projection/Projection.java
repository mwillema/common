package com.marcowillemart.common.projection;

import com.google.protobuf.Message;

/**
 * Projection represents a mutable projection of events.
 *
 * @author Marco Willemart
 */
public interface Projection {

    /**
     * @requires event != null
     * @modifies this
     * @effects Dispatches the given event to this
     */
    void dispatch(Message event);
}
