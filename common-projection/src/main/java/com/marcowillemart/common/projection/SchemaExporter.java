package com.marcowillemart.common.projection;

/**
 * SchemaExporter is used to exports the persistence schema of a query model if
 * the existing one is not valid anymore.
 *
 * @author Marco Willemart
 */
public interface SchemaExporter {

    /**
     * @return true iff the existing schema is valid
     */
    boolean isSchemaValid();

    /**
     * @modifies this
     * @effects Exports the schema
     */
    void export();
}
