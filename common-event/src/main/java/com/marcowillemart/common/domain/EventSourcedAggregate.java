package com.marcowillemart.common.domain;

import com.google.protobuf.Message;
import com.marcowillemart.common.util.Assert;
import com.marcowillemart.common.util.FailureException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * EventSourcedAggregate is an aggregate base class providing event-sourcing
 * commonality.
 *
 * @specfield version : int           // The version of the aggregate.
 * @specfield changes : List<Message> // The change list of the aggregate.
 *
 * @invariant version >= 0
 *
 * @author Marco Willemart
 */
public abstract class EventSourcedAggregate extends Aggregate {

    private static final int INITIAL_VERSION = 0;
    private static final String MUTATOR_METHOD_NAME = "when";
    private static final Map<String, Method> MUTATOR_METHODS = new HashMap<>();

    private final int version;
    private final List<Message> changes;

    /*
     * Abstraction Function:
     *   AF_super
     *   version = version
     *   changes = changes
     *
     * Representation Invariant:
     *   RI_super
     *   version >= 0
     *   changes != null
     */

    /**
     * @requires identity not null
     * @effects Makes this be a new event sourced aggregate a with
     *          a.identity = identity.id, a.version = 0 and a.changes = []
     */
    protected EventSourcedAggregate(Identity identity) {
        super(identity);

        this.version = INITIAL_VERSION;
        this.changes = new LinkedList<>();
    }

    /**
     * @requires identity not null and not empty &&
     *           version > 0 &&
     *           events not null and not empty && no null element in events
     * @effects Makes this be a new event sourced aggregate a with
     *          a.identity = identity, a.version = version, a.changes = [] and
     *          rehydrated with the given events.
     */
    protected EventSourcedAggregate(
            String identity,
            int version,
            Iterable<Message> events) {

        super(identity);

        Assert.isTrue(version > INITIAL_VERSION);
        Assert.notNull(events);

        this.version = version;
        this.changes = new LinkedList<>();

        boolean empty = true;

        for (Message event : events) {
            Assert.notNull(event);

            empty = false;
            mutate(event);
        }

        Assert.isFalse(empty);
    }

    /**
     * @return this.version
     */
    public final int version() {
        return version;
    }

    /**
     * @return this.changes
     */
    public final List<Message> changes() {
        return Collections.unmodifiableList(changes);
    }

    /**
     * @requires event != null
     * @modifies this
     * @effects Appends 'event' to this.changes and mutates this according to
     *          'event'.
     */
    protected final void apply(Message event) {
        Assert.notNull(event);

        changes.add(event);

        mutate(event);
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    /**
     * @requires event != null
     * @modifies this
     * @effects Anything, i.e., mutates this according to 'event'.
     */
    private void mutate(Message event) {
        Class<? extends EventSourcedAggregate> aggregateType = getClass();
        Class<? extends Message> eventType = event.getClass();

        String key = String.format(
                "%s:%s",
                aggregateType.getName(),
                eventType.getName());

        if (!MUTATOR_METHODS.containsKey(key)) {
            cacheMutatorMethodFor(key, aggregateType, eventType);
        }

        Method mutatorMethod = MUTATOR_METHODS.get(key);

        try {
            mutatorMethod.invoke(this, event);
        } catch (ReflectiveOperationException | IllegalArgumentException ex) {
            throw new FailureException(
                    String.format(
                            "Method cannot be invoked: %s.%s(%s)",
                            aggregateType.getSimpleName(),
                            MUTATOR_METHOD_NAME,
                            eventType.getSimpleName()),
                    ex);
        }
    }

    /**
     * @requires key != null && aggregateType != null && eventType != null
     * @modifies this
     * @effects Puts a new entry <k,v> into the cache of mutator methods where
     *          k = key and v is the mutator method of aggregate 'aggregateType'
     *          for the event 'eventType'.
     */
    private static void cacheMutatorMethodFor(
            String key,
            Class<? extends EventSourcedAggregate> aggregateType,
            Class<? extends Message> eventType) {

        try {
            Method method =
                    aggregateType.getDeclaredMethod(
                            MUTATOR_METHOD_NAME,
                            eventType);

            method.setAccessible(true);

            MUTATOR_METHODS.put(key, method);
        } catch (NoSuchMethodException ex) {
            throw new FailureException(
                    String.format(
                            "No such method: %s.%s(%s)",
                            aggregateType.getSimpleName(),
                            MUTATOR_METHOD_NAME,
                            eventType.getSimpleName()),
                    ex);
        }
    }
}
