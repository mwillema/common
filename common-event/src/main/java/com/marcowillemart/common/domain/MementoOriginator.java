package com.marcowillemart.common.domain;

import com.google.protobuf.Message;

/**
 * MementoOriginator defines that the implementor can create memento objects
 * (snapshots), that can be used to recreate the original state.
 *
 * @author Marco Willemart
 */
public interface MementoOriginator {

    /**
     * @return an opaque memento object that can be used to restore the state.
     */
    Message saveToMemento();
}
