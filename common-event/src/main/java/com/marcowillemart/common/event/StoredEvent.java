package com.marcowillemart.common.event;

import com.google.protobuf.Message;
import com.marcowillemart.common.util.Assert;
import com.marcowillemart.common.util.MessageSerializer;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;

/**
 * StoredEvent represents an immutable stored event.
 *
 * @specfield eventId : long                 // The unique ID of the stored
 *                                              event.
 * @specfield eventType : String             // The type name of the stored
 *                                              event.
 * @specfield eventBody : byte[]             // The serialized body of the
 *                                              stored event.
 * @specfield occurredOn : date              // The date and time of the
 *                                              occurrence of the stored event.
 * @specfield streamId : EventStreamId [0-1] // The stream ID of the stored
 *                                              event, if any.
 *
 * @author Marco Willemart
 */
public final class StoredEvent {

    private final long eventId;
    private final String eventType;
    private final byte[] eventBody;
    private final LocalDateTime occurredOn;
    private final EventStreamId streamId;

    /**
     * @requires eventType != null && eventBody != null &&
     *           'eventBody' is a valid serialization of an event of type
     *           'eventType' && the type named 'eventType' extends Message &&
     *           occurredOn != null
     * @effects Makes this be a new stored event e with e.eventId = eventId,
     *          e.eventType = eventType, e.eventBody = eventBody,
     *          e.occurredOn = occurredOn and e.streamId = nil.
     */
    public StoredEvent(
            long eventId,
            String eventType,
            byte[] eventBody,
            LocalDateTime occurredOn) {

        this(eventId, eventType, eventBody, occurredOn, null);
    }

    /**
     * @requires eventType != null && eventBody != null &&
     *           'eventBody' is a valid serialization of an event of type
     *           'eventType' && the type named 'eventType' extends Message &&
     *           occurredOn != null
     * @effects Makes this be a new stored event e with e.eventId = eventId,
     *          e.eventType = eventType, e.eventBody = eventBody,
     *          e.occurredOn = occurredOn and e.streamId = streamId.
     */
    public StoredEvent(
            long eventId,
            String eventType,
            byte[] eventBody,
            LocalDateTime occurredOn,
            EventStreamId streamId) {

        Assert.notNull(eventType);
        Assert.notNull(eventBody);
        Assert.notNull(occurredOn);

        this.eventId = eventId;
        this.occurredOn = occurredOn;
        this.eventType = eventType;
        this.eventBody = eventBody.clone();
        this.streamId = streamId;
    }

    /**
     * @return this.eventId
     */
    public long eventId() {
        return eventId;
    }

    /**
     * @return this.eventType
     */
    public String eventType() {
        return eventType;
    }

    /**
     * @return this.eventBody
     */
    public byte[] eventBody() {
        return eventBody.clone();
    }

    /**
     * @return this.occurredOn
     */
    public LocalDateTime occurredOn() {
        return occurredOn;
    }

    /**
     * @return true iff this.streamId != nil
     */
    public boolean hasStreamId() {
        return streamId != null;
    }

    /**
     * @requires hasStreamId()
     * @return this.streamId
     */
    public EventStreamId streamId() {
        Assert.isTrue(hasStreamId());

        return streamId;
    }

    /**
     * @return this.eventBody as a domain event of type T, T being the type
     *         named this.eventType.
     */
    public <T extends Message> T toDomainEvent() {
        return MessageSerializer.deserialize(eventType, eventBody);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (int) (eventId ^ (eventId >>> 32));
        hash = 29 * hash + occurredOn.hashCode();
        hash = 29 * hash + eventType.hashCode();
        hash = 29 * hash + Arrays.hashCode(eventBody);
        hash = 29 * hash + Objects.hashCode(streamId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof StoredEvent)) {
            return false;
        }
        final StoredEvent other = (StoredEvent) obj;
        return eventId == other.eventId
                && eventType.equals(other.eventType)
                && Arrays.equals(eventBody, other.eventBody)
                && occurredOn.equals(other.occurredOn)
                && Objects.equals(streamId, other.streamId);
    }

    @Override
    public String toString() {
        return "StoredEvent{"
                + "eventId=" + eventId
                + ", eventType=" + eventType
                + ", eventBody=" + Arrays.toString(eventBody)
                + ", occurredOn=" + occurredOn
                + ", streamId=" + streamId
                + '}';
    }
}
