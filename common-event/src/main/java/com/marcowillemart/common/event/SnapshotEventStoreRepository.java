package com.marcowillemart.common.event;

import com.google.protobuf.Message;
import com.marcowillemart.common.domain.EventSourcedAggregate;
import com.marcowillemart.common.domain.Identity;
import com.marcowillemart.common.domain.MementoOriginator;
import com.marcowillemart.common.util.Assert;
import java.util.ArrayList;
import java.util.List;

/**
 * SnapshotEventStoreRepository is an event store repository with snapshotting
 * capabilities.
 *
 * By default a snapshot is taken every time a predetermined number of events is
 * reached. However this logic can be customized by overriding the
 * shouldTakeSnapshot(T) method.
 *
 * @author Marco Willemart
 */
public abstract class SnapshotEventStoreRepository
        <T extends EventSourcedAggregate & MementoOriginator,
            I extends Identity>
        extends EventStoreRepository<T, I> {

    private final SnapshotRepository snapshotRepository;
    private final int nbOfEvents;

    /**
     * @requires eventStore != null && snapshotRepository != null &&
     *           aggregateType != null && nbOfEvents > 0
     * @effects Makes this be a new snapshot event store repository with the
     *          given event store for the aggregate of the given type and that
     *          takes a snapshot every 'nbOfEvents' number of events.
     */
    protected SnapshotEventStoreRepository(
            EventStore eventStore,
            SnapshotRepository snapshotRepository,
            Class<T> aggregateType,
            int nbOfEvents) {

        super(eventStore, aggregateType);

        Assert.notNull(snapshotRepository);
        Assert.isTrue(nbOfEvents > 0);

        this.snapshotRepository = snapshotRepository;
        this.nbOfEvents = nbOfEvents;
    }

    @Override
    public void save(T aggregate) {
        super.save(aggregate);

        if (shouldTakeSnapshot(aggregate)) {
            Snapshot snapshot =
                    new Snapshot(
                            streamNameFor(aggregate.identity()),
                            lastVersionOf(aggregate),
                            aggregate.saveToMemento());

            snapshotRepository.save(snapshot);
        }
    }

    @Override
    public T find(I identity) {
        String streamName = streamNameFor(identity.id());

        if (snapshotRepository.exists(streamName)) {
            Snapshot snapshot = snapshotRepository.find(streamName);

            EventStream eventStream =
                    eventStore().loadEventStreamAfter(
                            streamName,
                            snapshot.version());

            return aggregateFrom(
                    identity.id(),
                    eventStream.version(),
                    concat(snapshot.memento(), eventStream.events()));
        } else {
            return super.find(identity);
        }
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    /**
     * @requires aggregate != null
     * @return true iff a snapshot of the given aggregate should be taken
     */
    protected boolean shouldTakeSnapshot(T aggregate) {
        return lastVersionOf(aggregate) % nbOfEvents == 0;
    }

    /**
     * @requires aggregate != null
     * @return aggregate.version + aggregate.changes.size
     */
    protected final int lastVersionOf(T aggregate) {
        return aggregate.version() + aggregate.changes().size();
    }

    /**
     * @requires memento != null && events != null && no null element in events
     * @return [memento] + events
     */
    private static List<Message> concat(Message memento, List<Message> events) {
        List<Message> list = new ArrayList<>(1 + events.size());

        list.add(memento);
        list.addAll(events);

        return list;
    }
}
