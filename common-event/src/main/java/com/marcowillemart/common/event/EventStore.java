package com.marcowillemart.common.event;

import com.google.protobuf.Message;
import java.util.List;
import java.util.function.Consumer;

/**
 * EventStore represents a mutable event store.
 *
 * @author Marco Willemart
 */
public interface EventStore {

    ////////////////////
    // STORED EVENTS
    ////////////////////

    /**
     * @requires event != null
     * @modifies this
     * @effects Stores the given event in a new stored event se and appends se
     *          to this.
     * @return se, i.e., the created stored event.
     */
    StoredEvent append(Message event);

    /**
     * @requires storedEvent != null &&
     *           no stored event identified by storedEvent.eventId in this
     * @modifies this
     * @effects Appends the given stored event to this.
     */
    @Deprecated
    void append(StoredEvent storedEvent);

    /**
     * @return the number of stored events in this.
     */
    long countStoredEvents();

    /**
     * @return the id of the last event stored in this if any, else returns 0.
     */
    long lastStoredEventId();

    /**
     * @return the stored events evts such that for all e in evts, e.eventId in
     *         [lowEventId..highEventId] and e is sorted by its id in ascending
     *         order.
     */
    List<StoredEvent> allStoredEventsBetween(long lowEventId, long highEventId);

    /**
     * @return the stored events evts such that for all e in evts,
     *         e.eventId >= eventId and all e are sorted by their id in
     *         ascending order.
     */
    List<StoredEvent> allStoredEventsSince(long eventId);

    ////////////////////
    // EVENT STREAMS
    ////////////////////

    /**
     * @requires streamName not null and not empty &&
     *           expectedVersion >= 0 &&
     *           events not null and does not contain any null element
     * @modifies this
     * @effects Appends the given events to the stream named streamName in this
     * @throws EventStoreConcurrencyException if the version at which we
     *         currently expect the stream to be is different than
     *         expectedVersion
     */
    void appendToStream(
            String streamName,
            int expectedVersion,
            Iterable<Message> events) throws EventStoreConcurrencyException;

    /**
     * @requires streamName != null
     * @return true iff an event stream named streamName exists in this
     */
    boolean eventStreamExists(String streamName);

    /**
     * @requires streamName != null
     * @return the event stream named streamName in this
     */
    EventStream loadEventStream(String streamName);

    /**
     * @requires streamName != null && version >= 0
     * @return the event stream named streamName in this starting from
     *         version + 1
     */
    EventStream loadEventStreamAfter(String streamName, int version);

    ////////////////////
    // SUBSCRIPTIONS
    ////////////////////

    /**
     * @requires afterEventId >= 0 && subscriber != null
     * @modifies this
     * @effects Delivers all the events after the given event ID to the given
     *          subscriber, as well as all events that will be appended.
     */
    void subscribeToAll(long afterEventId, Consumer<Message> subscriber);
}
