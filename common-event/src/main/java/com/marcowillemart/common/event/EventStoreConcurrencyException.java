package com.marcowillemart.common.event;

import com.google.protobuf.Message;
import com.marcowillemart.common.util.Assert;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * EventStoreConcurrencyException is an unchecked exception that is thrown when
 * multiple users attempt to modify the same event stream.
 *
 * @specfield name : String                // The name of the stream
 * @specfield expectedVersion : int        // The expected version of the stream
 * @specfield actualVersion : int          // The actual version of the stream
 * @specfield actualEvents : List<Message> // The events that were concurrently
 *                                            appended to the stream
 *
 * @invariant name not empty
 * @invariant expectedVersion < actualVersion
 * @invariant actualEvents not empty
 *
 * @author Marco Willemart
 */
public final class EventStoreConcurrencyException extends RuntimeException {

    private final String name;
    private final int expectedVersion;
    private final int actualVersion;
    private final List<Message> actualEvents;

    /**
     * Abstraction Function:
     *   name = name
     *   expectedVersion = expectedVersion
     *   actualVersion = actualVersion
     *   actualEvents = actualEvents
     *
     * Representation Invariant:
     *   name != null
     *   name not empty
     *   expectedVersion < actualVersion
     *   actualEvents != null
     *   actualEvents not empty
     */

    /**
     * @effects Asserts the rep invariant holds for this.
     */
    private void checkRep() {
        Assert.notEmpty(name);
        Assert.isTrue(expectedVersion < actualVersion);
        Assert.notEmpty(actualEvents);
    }

    /**
     * @requires name != null && name not empty &&
     *           expectedVersion < actualVersion && actualEvents != null &&
     *           actualEvents not empty
     * @effects Makes this be a new ConcurrencyException e with
     *          e.name = name,
     *          e.expectedVersion = expectedVersion,
     *          e.actualVersion = actualVersion,
     *          e.actualEvents = actualEvents and
     *          e.cause = nil
     */
    public EventStoreConcurrencyException(
            String name,
            int expectedVersion,
            int actualVersion,
            List<Message> actualEvents) {

        this(name, expectedVersion, actualVersion, actualEvents, null);
    }

    /**
     * @requires name != null && name not empty &&
     *           expectedVersion < actualVersion && actualEvents != null &&
     *           actualEvents not empty
     * @effects Makes this be a new ConcurrencyException e with
     *          e.name = name,
     *          e.expectedVersion = expectedVersion,
     *          e.actualVersion = actualVersion,
     *          e.actualEvents = actualEvents and
     *          e.cause = cause
     */
    public EventStoreConcurrencyException(
            String name,
            int expectedVersion,
            int actualVersion,
            List<Message> actualEvents,
            Throwable cause) {

        super(String.format(
                "Expected v%d but found v%d in stream '%s'",
                expectedVersion,
                actualVersion,
                name),
                cause);

        this.name = name;
        this.expectedVersion = expectedVersion;
        this.actualVersion = actualVersion;
        this.actualEvents = new ArrayList<>(actualEvents);

        checkRep();
    }

    /**
     * @return this.name
     */
    public String name() {
        return name;
    }

    /**
     * @return this.expectedVersion
     */
    public int expectedVersion() {
        return expectedVersion;
    }

    /**
     * @return this.actualVersion
     */
    public int actualVersion() {
        return actualVersion;
    }

    /**
     * @return this.actualEvents
     */
    public List<Message> actualEvents() {
        return Collections.unmodifiableList(actualEvents);
    }
}
