package com.marcowillemart.common.event;

import com.marcowillemart.common.domain.Repository;

/**
 * Snapshot repository represents a mutable repository of snapshots.
 *
 * @author Marco Willemart
 */
public interface SnapshotRepository extends Repository<Snapshot, String> {
}
