package com.marcowillemart.common.event;

import com.marcowillemart.common.util.Assert;
import org.springframework.context.ApplicationEventPublisher;

/**
 * FollowStoreEventDispatcher follows an event store and dispatches the newly
 * appended events to the listeners registered with this application.
 *
 * @author Marco Willemart
 */
public class FollowStoreEventDispatcher {

    /**
     * @requires all params are not null
     * @effects Makes this be a new FollowEventStoreDispatcher with the given
     *          dependencies.
     */
    public FollowStoreEventDispatcher(
            EventStore eventStore,
            ApplicationEventPublisher publisher) {

        Assert.notNull(eventStore);
        Assert.notNull(publisher);

        eventStore.subscribeToAll(
                eventStore.lastStoredEventId(),
                publisher::publishEvent);
    }
}
