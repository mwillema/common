package com.marcowillemart.common.event;

import com.google.protobuf.Message;
import com.marcowillemart.common.domain.EventSourcedAggregate;
import com.marcowillemart.common.domain.Identity;
import com.marcowillemart.common.domain.Repository;
import com.marcowillemart.common.util.Assert;
import com.marcowillemart.common.util.FailureException;
import java.lang.reflect.Constructor;
import java.util.List;

/**
 * EventStoreRepository is a base implementation of an event store repository
 * for aggregates of type T that have a unique identity of type I.
 *
 * @author Marco Willemart
 */
public abstract class EventStoreRepository
        <T extends EventSourcedAggregate, I extends Identity>
        implements Repository<T, I> {

    private static final String CONSTRUCTOR_PARAMS =
            "String, int, Iterable<Message>";

    private final EventStore eventStore;
    private final Constructor<T> constructor;

    /**
     * @requires eventStore != null && aggregateType != null
     * @effects Makes this be a new event store repository with the
     *          given event store for the aggregate of the given type.
     */
    protected EventStoreRepository(
            EventStore eventStore,
            Class<T> aggregateType) {

        Assert.notNull(eventStore);
        Assert.notNull(aggregateType);

        this.eventStore = eventStore;
        this.constructor = constructorOf(aggregateType);
    }

    @Override
    public void save(T aggregate) {
        Assert.notNull(aggregate);

        String streamName = streamNameFor(aggregate.identity());

        eventStore.appendToStream(
                streamName,
                aggregate.version(),
                aggregate.changes());
    }

    @Override
    public boolean exists(I identity) {
        Assert.notNull(identity);

        String streamName = streamNameFor(identity.id());

        return eventStore.eventStreamExists(streamName);
    }

    @Override
    public T find(I identity) {
        Assert.notNull(identity);
        //Assert.isTrue(exists(identity));

        String streamName = streamNameFor(identity.id());

        EventStream eventStream = eventStore.loadEventStream(streamName);

        return aggregateFrom(
                identity.id(),
                eventStream.version(),
                eventStream.events());
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    /**
     * @return the event store of this
     */
    protected EventStore eventStore() {
        return eventStore;
    }

    /**
     * @requires identity != null
     * @return the stream name for the given aggregate identity.
     */
    protected String streamNameFor(String identity) {
        return identity;
    }

    /**
     * @requires identity not null and not empty && version > 0 &&
     *           events not null and not empty && no null element in events
     * @return a new aggregate a with a.identity = id, a.version = version,
     *         a.changes = [] and rehydrated with the given events.
     */
    protected T aggregateFrom(
            String identity,
            int version,
            List<Message> events) {

        try {
            return constructor.newInstance(identity, version, events);
        } catch (ReflectiveOperationException | IllegalArgumentException ex) {
            throw new FailureException(
                    String.format(
                            "Constructor cannot be invoked: %s(%s)",
                            constructor.getName(),
                            CONSTRUCTOR_PARAMS),
                    ex);
        }
    }

    /**
     * @requires aggregateType != null && aggregateType has a constructor with
     *           parameters of type String, int, and List<Message>
     * @return the constructor of aggregateType with parameters String, int, and
     *         List<Message>
     */
    private static <T> Constructor<T> constructorOf(Class<T> aggregateType) {
        try {
            return aggregateType.getDeclaredConstructor(
                    String.class,
                    Integer.TYPE,
                    Iterable.class);
        } catch (NoSuchMethodException ex) {
            throw new FailureException(
                    String.format(
                            "No such constructor: %s(%s)",
                            aggregateType.getSimpleName(),
                            CONSTRUCTOR_PARAMS),
                    ex);
        }
    }
}
