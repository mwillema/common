package com.marcowillemart.common.event;

import com.google.protobuf.Message;
import com.marcowillemart.common.util.Assert;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * EventStream represents an immutable stream of events.
 *
 * @specfield version : int          // The version of the stream.
 * @specfield events : List<Message> // The events of the stream.
 *
 * @invariant version >= 0
 *
 * @author Marco Willemart
 */
public final class EventStream {

    private final int version;
    private final List<Message> events;

    /*
     * Abstraction Function:
     *   version = version
     *   events = events
     *
     * Representation Invariant:
     *   version >= 0
     *   events != null
     *   for all e in events, e != null
     */

    /**
     * @requires version >= 0 && events not null && no null elements in events
     * @effects Makes this be a new event stream s with s.version = version and
     *          s.events = events
     */
    public EventStream(int version, List<Message> events) {
        Assert.isTrue(version >= 0);
        Assert.notNull(events);

        this.version = version;
        this.events = new ArrayList<>(events);
    }

    /**
     * @return this.version
     */
    public int version() {
        return version;
    }

    /**
     * @return this.events
     */
    public List<Message> events() {
        return Collections.unmodifiableList(events);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + version;
        hash = 23 * hash + events.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof EventStream)) {
            return false;
        }
        EventStream other = (EventStream) obj;
        return version == other.version && events.equals(other.events);
    }

    @Override
    public String toString() {
        return "EventStream{"
                + "version=" + version
                + ", events=" + events
                + '}';
    }
}
