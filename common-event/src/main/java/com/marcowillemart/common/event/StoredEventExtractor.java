package com.marcowillemart.common.event;

import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.Message;
import com.marcowillemart.common.util.Assert;
import com.marcowillemart.common.util.DataTable;
import com.marcowillemart.common.util.Strings;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * StoredEventExtractor is a stateless domain service responsible for extracting
 * data from stored events to a data table.
 *
 * @author Marco Willemart
 * @author Marco Willemart
 */
public class StoredEventExtractor {

    private static final String OVERVIEW = "Overview";
    private static final String EVENT_ID = "eventId";
    private static final String EVENT_TYPE = "eventType";
    private static final String OCCURRED_ON = "occurredOn";
    private static final String STREAM_NAME = "streamName";
    private static final String STREAM_VERSION = "streamVersion";

    /**
     * @effects Makes this be a new stored event extractor.
     */
    public StoredEventExtractor() {
    }

    /**
     * @requires storedEvents != null && no null element in storedEvents
     * @return a list of data tables created from the given stored events.
     */
    public List<DataTable> extract(List<StoredEvent> storedEvents) {
        Assert.noNullElement(storedEvents);

        List<DataTable> tables = new LinkedList<>();

        tables.add(overviewTableFrom(storedEvents));
        tables.addAll(eventTablesFrom(storedEvents));

        return tables;
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    /**
     * @requires storedEvents != null
     * @return a table containing an overview of all the given stored events
     */
    private static DataTable overviewTableFrom(List<StoredEvent> storedEvents) {
        DataTable table =
                new DataTable(
                        OVERVIEW,
                        Arrays.asList(
                                EVENT_ID,
                                EVENT_TYPE,
                                OCCURRED_ON,
                                STREAM_NAME,
                                STREAM_VERSION));

        for (StoredEvent storedEvent : storedEvents) {
            table.addRow(
                    Arrays.asList(
                            Long.toString(storedEvent.eventId()),
                            eventNameFrom(storedEvent),
                            storedEvent.occurredOn().toString(),
                            streamNameFrom(storedEvent),
                            streamVersionFrom(storedEvent)));
        }

        return table;
    }

    /**
     * @requires storedEvents != null
     * @return a collection of tables for each type of event from the given
     *         stored events
     */
    private static Collection<DataTable> eventTablesFrom(
            List<StoredEvent> storedEvents) {

        Map<String, DataTable> tables = new LinkedHashMap<>();

        for (StoredEvent storedEvent : storedEvents) {

            if (!tables.containsKey(storedEvent.eventType())) {
                DataTable dataTable =
                        new DataTable(
                                eventNameFrom(storedEvent),
                                fieldNamesFrom(storedEvent));

                tables.put(storedEvent.eventType(), dataTable);
            }

            tables.get(storedEvent.eventType())
                    .addRow(fieldValuesFrom(storedEvent));
        }

        return tables.values();
    }

    /**
     * @requires storedEvent != null
     * @return the event name of the given stored event
     */
    private static String eventNameFrom(StoredEvent storedEvent) {
        return storedEvent.toDomainEvent().getDescriptorForType().getName();
    }

    /**
     * @requires storedEvent != null
     * @return storedEvent.streamId.name if storedEvent.streamId != nil, else
     *         returns an empty string
     */
    private static String streamNameFrom(StoredEvent storedEvent) {
        if (storedEvent.hasStreamId()) {
            return storedEvent.streamId().name();
        }

        return Strings.EMPTY;
    }

    /**
     * @requires storedEvent != null
     * @return storedEvent.streamId.version (as String) if
     *         storedEvent.streamId != nil, else returns an empty string
     */
    private static String streamVersionFrom(StoredEvent storedEvent) {
        if (storedEvent.hasStreamId()) {
            return Integer.toString(storedEvent.streamId().version());
        }

        return Strings.EMPTY;
    }

    /**
     * @requires storedEvent not null
     * @return a new list containing the names of the fields of the given
     *         stored event
     */
    private static List<String> fieldNamesFrom(StoredEvent storedEvent) {
        List<String> names = new LinkedList<>();

        names.add(EVENT_ID);
        names.add(OCCURRED_ON);

        Message message = storedEvent.toDomainEvent();

        for (FieldDescriptor fieldDescriptor :
                message.getDescriptorForType().getFields()) {
            names.add(fieldDescriptor.getName());
        }

        return names;
    }

    /**
     * @requires storedEvent not null
     * @return a new list containing the values of the fields of the given
     *         stored event
     */
    private static List<String> fieldValuesFrom(StoredEvent storedEvent) {
        List<String> values = new LinkedList<>();

        values.add(Long.toString(storedEvent.eventId()));
        values.add(storedEvent.occurredOn().toString());

        Message message = storedEvent.toDomainEvent();

        for (FieldDescriptor fieldDescriptor :
                message.getDescriptorForType().getFields()) {
            values.add(message.getField(fieldDescriptor).toString());
        }

        return values;
    }
}
