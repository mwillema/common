package com.marcowillemart.common.event;

import com.marcowillemart.common.util.Assert;

/**
 * EventStreamId represents the immutable identity of an event stream.
 *
 * @specfield name : String // The name of the event stream id.
 * @specfield version : int // The version of the event stream id.
 *
 * @invariant name.length > 0
 * @invariant version >= 1
 *
 * @author Marco Willemart
 */
public final class EventStreamId {

    private static final int INITIAL_VERSION = 1;

    private final String name;
    private final int version;

    /*
     * Abstraction Function:
     *   name = name
     *   version = version
     *
     * Representation Invariant:
     *   name != null
     *   name.length > 0
     *   version >= 1
     */

    /**
     * @effects Asserts the rep invariant holds for this.
     */
    private void checkRep() {
        Assert.notEmpty(name);
        Assert.isTrue(version >= 1);
    }

    /**
     * @requires name != null && name.length > 0
     * @effects Makes this be a new event stream id i with i.name = name and
     *          i.version = 1
     */
    public EventStreamId(String name) {
        this(name, INITIAL_VERSION);
    }

    /**
     * @requires name != null && name.length > 0 && version >= 1
     * @effects Makes this be a new event stream id i with i.name = name and
     *          i.version = version
     */
    public EventStreamId(String name, int version) {
        this.name = name;
        this.version = version;

        checkRep();
    }

    /**
     * @requires nameSegment1 and nameSegment2 are not null and not empty
     * @effects Makes this be a new event stream id i with
     *          i.name = nameSegment1 + ':' + nameSegment2 and i.version = 1
     */
    public EventStreamId(String nameSegment1, String nameSegment2) {
        this(nameSegment1, nameSegment2, INITIAL_VERSION);
    }

    /**
     * @requires nameSegment1 and nameSegment2 are not null and not empty &&
     *           version >= 1
     * @effects Makes this be a new event stream id i with
     *          i.name = nameSegment1 + ':' + nameSegment2 and
     *          i.version = version
     */
    public EventStreamId(String nameSegment1, String nameSegment2,
            int version) {
        this(String.format("%s:%s", nameSegment1, nameSegment2), version);
    }

    /**
     * @return this.name
     */
    public String name() {
        return name;
    }

    /**
     * @return this.version
     */
    public int version() {
        return version;
    }

    /**
     * @requires version >= 1
     * @return a new event stream id i with i.name = this.name and
     *         i.version = version
     */
    public EventStreamId withVersion(int version) {
        return new EventStreamId(name, version);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + name.hashCode();
        hash = 17 * hash + version;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof EventStreamId)) {
            return false;
        }
        EventStreamId other = (EventStreamId) obj;
        return name.equals(other.name) && version == other.version;
    }

    @Override
    public String toString() {
        return "EventStreamId{" + "name=" + name + ", version=" + version + '}';
    }
}
