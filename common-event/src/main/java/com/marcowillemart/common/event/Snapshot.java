package com.marcowillemart.common.event;

import com.google.protobuf.Message;
import com.marcowillemart.common.util.Assert;

/**
 * Snapshot represents an immutable snapshot of an aggregate.
 *
 * @specfield identity : String // The identity of the aggregate.
 * @specfield version :int      // The version of the aggregate.
 * @specfield memento : Message // The memento of the state of the aggregate.
 *
 * @invariant identity not empty
 * @invariant version > 0
 *
 * @author Marco Willemart
 */
public final class Snapshot {

    private final String identity;
    private final int version;
    private final Message memento;

    /**
     * @requires identity not null and not empty && version > 0 &&
     *           memento != null
     * @effects Makes this be a new Snapshot s with s.identity = identity,
     *          s.version = version and s.memento = memento
     */
    public Snapshot(String identity, int version, Message memento) {
        Assert.notEmpty(identity);
        Assert.isTrue(version > 0);
        Assert.notNull(memento);

        this.identity = identity;
        this.version = version;
        this.memento = memento;
    }

    /**
     * @return this.identity
     */
    public String identity() {
        return identity;
    }

    /**
     * @return this.version
     */
    public int version() {
        return version;
    }

    /**
     * @return this.memento
     */
    public Message memento() {
        return memento;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + identity.hashCode();
        hash = 43 * hash + version;
        hash = 43 * hash + memento.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Snapshot)) {
            return false;
        }
        final Snapshot other = (Snapshot) obj;
        return identity.equals(other.identity)
                && version == other.version
                && memento.equals(other.memento);
    }

    @Override
    public String toString() {
        return "Snapshot{"
                + "identity=" + identity
                + ", version=" + version
                + ", memento=" + memento
                + '}';
    }
}
