package com.marcowillemart.common.infrastructure.persistence.event;

import com.marcowillemart.common.concurrent.ThreadSafe;
import com.marcowillemart.common.event.Snapshot;
import com.marcowillemart.common.event.SnapshotRepository;
import com.marcowillemart.common.util.Assert;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@ThreadSafe
public final class InMemorySnapshotRepository implements SnapshotRepository {

    private final ConcurrentMap<String, Snapshot> snapshots;

    public InMemorySnapshotRepository() {
        this.snapshots = new ConcurrentHashMap<>();
    }

    @Override
    public void save(Snapshot aggregate) {
        Assert.notNull(aggregate);

        snapshots.put(aggregate.identity(), aggregate);
    }

    @Override
    public boolean exists(String identity) {
        Assert.notNull(identity);

        return snapshots.containsKey(identity);
    }

    @Override
    public Snapshot find(String identity) {
        Assert.notNull(identity);

        Snapshot snapshot = snapshots.get(identity);

        Assert.notNull(snapshot);
        return snapshot;
    }
}
