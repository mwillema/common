package com.marcowillemart.common.infrastructure.persistence.event;

import com.google.protobuf.Message;
import com.marcowillemart.common.concurrent.GuardedBy;
import com.marcowillemart.common.concurrent.ThreadSafe;
import com.marcowillemart.common.event.EventStore;
import com.marcowillemart.common.event.EventStoreConcurrencyException;
import com.marcowillemart.common.event.EventStream;
import com.marcowillemart.common.event.StoredEvent;
import com.marcowillemart.common.util.Assert;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * In-memory implementation of the EvenStore interface.
 *
 * @author Marco Willemart
 */
@ThreadSafe
public final class InMemoryEventStore implements EventStore {

    @GuardedBy("this") private long lastStoredEventId;
    @GuardedBy("this") private final Map<Long, StoredEvent> storedEvents;
    @GuardedBy("this") private final Map<String, List<Message>> eventStreams;
    @GuardedBy("this") private Optional<Consumer<Message>> subscriber;

    /**
     * @effects Makes this be a new empty memory event store.
     */
    public InMemoryEventStore() {
        this.lastStoredEventId = 0;
        this.storedEvents = new LinkedHashMap<>();
        this.eventStreams = new LinkedHashMap<>();
        this.subscriber = Optional.empty();
    }

    /**
     * @modifies this
     * @effects Clears all the event stored in this
     */
    public void clear() {
        synchronized (this) {
            lastStoredEventId = 0;
            storedEvents.clear();
            eventStreams.clear();
            subscriber = Optional.empty();
        }
    }

    /**
     * @requires storedEvent != null &&
     *           storedEvent.eventId greater than the last stored event id
     * @modifies this
     * @effects Appends storedEvent in this
     */
    @Override
    public void append(StoredEvent storedEvent) {
        Assert.notNull(storedEvent);

        synchronized (this) {
            Assert.isTrue(storedEvent.eventId() > lastStoredEventId);

            lastStoredEventId = storedEvent.eventId();
            storedEvents.put(storedEvent.eventId(), storedEvent);

            subscriber.ifPresent(s -> s.accept(storedEvent.toDomainEvent()));
        }
    }

    @Override
    public StoredEvent append(Message event) {
        synchronized (this) {
            StoredEvent storedEvent =
                    new StoredEvent(
                            lastStoredEventId + 1,
                            event.getClass().getName(),
                            event.toByteArray(),
                            LocalDateTime.now());

            append(storedEvent);

            return storedEvent;
        }
    }

    @Override
    public long countStoredEvents() {
        synchronized (this) {
            return storedEvents.size();
        }
    }

    @Override
    public long lastStoredEventId() {
        synchronized (this) {
            return lastStoredEventId;
        }
    }

    @Override
    public List<StoredEvent> allStoredEventsBetween(
            long lowEventId,
            long highEventId) {

        List<StoredEvent> subList = new LinkedList<>();

        synchronized (this) {
            for (Entry<Long, StoredEvent> entry : storedEvents.entrySet()) {
                long eventId = entry.getKey();

                if (lowEventId <= eventId && eventId <= highEventId) {
                    subList.add(entry.getValue());
                }
            }
        }

        return subList;
    }

    @Override
    public List<StoredEvent> allStoredEventsSince(long eventId) {
        synchronized (this) {
            return allStoredEventsBetween(eventId, lastStoredEventId);
        }
    }

    @Override
    public void appendToStream(
            String streamName,
            int expectedVersion,
            Iterable<Message> events) throws EventStoreConcurrencyException {

        Assert.notEmpty(streamName);
        Assert.isTrue(expectedVersion >= 0);
        Assert.notNull(events);

        synchronized (this) {
            if (!eventStreams.containsKey(streamName)) {
                eventStreams.put(streamName, new ArrayList<>());
            }

            List<Message> eventStream = eventStreams.get(streamName);
            int actualVersion = eventStream.size();

            if (expectedVersion < actualVersion) {
                throw new EventStoreConcurrencyException(
                        streamName,
                        expectedVersion,
                        actualVersion,
                        eventStream.subList(
                                expectedVersion,
                                actualVersion));
            }

            // TODO: Handle the case where expectedVersion > actualVersion
            Assert.equals(expectedVersion, actualVersion);

            events.forEach(event -> {
                append(event);
                eventStream.add(event);
            });
        }
    }

    @Override
    public boolean eventStreamExists(String streamName) {
        Assert.notNull(streamName);

        synchronized (this) {
            return eventStreams.containsKey(streamName);
        }
    }

    @Override
    public EventStream loadEventStream(String streamName) {
        return loadEventStreamAfter(streamName, 0);
    }

    @Override
    public EventStream loadEventStreamAfter(String streamName, int version) {
        Assert.notNull(streamName);
        Assert.isTrue(version >= 0);

        synchronized (this) {
            List<Message> eventStream =
                    eventStreams.getOrDefault(
                            streamName,
                            Collections.emptyList());

            if (version >= eventStream.size()) {
                return new EventStream(version, Collections.emptyList());
            }

            return new EventStream(
                    eventStream.size(),
                    eventStream.subList(version, eventStream.size()));
        }
    }

    @Override
    public void subscribeToAll(
            long afterEventId,
            Consumer<Message> subscriber) {

        Assert.isTrue(afterEventId >= 0);
        Assert.notNull(subscriber);

        synchronized (this) {
            this.subscriber = Optional.of(subscriber);

            allStoredEventsSince(afterEventId + 1)
                    .forEach(storedEvent ->
                            this.subscriber.ifPresent(
                                    s -> s.accept(
                                            storedEvent.toDomainEvent())));
        }
    }
}
