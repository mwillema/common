package com.marcowillemart.common.event.test;

import com.google.protobuf.Message;
import com.marcowillemart.common.domain.EventSourcedAggregate;
import com.marcowillemart.common.domain.MementoOriginator;
import com.marcowillemart.common.test.Event.DummyCreated;
import com.marcowillemart.common.test.Event.DummyUpdated;
import com.marcowillemart.common.test.Memento.DummyMemento;
import com.marcowillemart.common.util.Assert;

/**
 * Dummy is a dummy aggregate.
 *
 * Implementing MementoOriginator is only needed if snapshotting must be
 * supported.
 *
 * @author Marco Willemart
 */
public final class Dummy
        extends EventSourcedAggregate
        implements MementoOriginator {

    private DummyId dummyId;
    private int value;

    public Dummy(String identity, int version, Iterable<Message> events) {
        super(identity, version, events);
    }

    public Dummy(DummyId dummyId) {
        super(dummyId);

        apply(DummyCreated.newBuilder()
                .setId(dummyId.id())
                .build());
    }

    public void update(int value) {
        apply(DummyUpdated.newBuilder()
                .setValue(value)
                .build());
    }

    public DummyId dummyId() {
        return dummyId;
    }

    public int value() {
        return value;
    }

    private void when(DummyCreated event) {
        Assert.isNull(dummyId);

        dummyId = new DummyId(event.getId());
        value = 0;
    }

    private void when(DummyUpdated event) {
        Assert.notNull(dummyId);

        value = event.getValue();
    }

    ////////////////////
    // OPTIONAL SNAPSHOT SUPPORT
    ////////////////////

    @Override
    public Message saveToMemento() {
        return DummyMemento.newBuilder()
                .setId(dummyId.id())
                .setValue(value)
                .build();
    }

    private void when(DummyMemento memento) {
        dummyId = new DummyId(memento.getId());
        value = memento.getValue();
    }
}
