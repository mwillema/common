package com.marcowillemart.common.event;

import static com.marcowillemart.common.event.test.Tests.*;
import com.marcowillemart.common.test.Dto;
import static com.marcowillemart.common.test.Tests.*;
import java.time.LocalDateTime;
import java.util.Arrays;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for the StoredEvent class.
 *
 * @author Marco Willemart
 */
public class StoredEventTest {

    private static final long EVENT_ID = 5;
    private static final String EVENT_TYPE = Dto.Sample.class.getName();

    private StoredEvent target;
    private StoredEvent same;
    private StoredEvent different1;
    private StoredEvent different2;

    private LocalDateTime now;
    private Dto.Sample event;
    private byte[] eventBody;
    private EventStreamId streamId;

    @Before
    public void setUp() {
        now = LocalDateTime.now();
        event = createAnonymousDtoSample();
        eventBody = event.toByteArray();
        streamId = createAnonymousEventStreamId();

        target = new StoredEvent(EVENT_ID, EVENT_TYPE, eventBody, now,
                streamId);
        same = new StoredEvent(EVENT_ID, EVENT_TYPE, eventBody, now, streamId);
        different1 = new StoredEvent(8, "event", "other".getBytes(),
                now.plusDays(3), streamId.withVersion(10));
        different2 = new StoredEvent(EVENT_ID, EVENT_TYPE, eventBody, now);
    }

    @Test
    public void testEventId() {
        // Exercice & Verify
        assertEquals(EVENT_ID, target.eventId());
    }

    @Test
    public void testEventType() {
        // Exercice & Verify
        assertEquals(EVENT_TYPE, target.eventType());
    }

    @Test
    public void testEventBody() {
        // Exercice & Verify
        assertArrayEquals(eventBody, target.eventBody());
    }

    @Test
    public void testOccuredOn() {
        // Exercice & Verify
        assertEquals(now, target.occurredOn());
    }

    @Test
    public void testHasStreamId_true() {
        // Exercise & Verify
        assertTrue(target.hasStreamId());
    }

    @Test
    public void testHasStreamId_false() {
        // Exercise & Verify
        assertFalse(different2.hasStreamId());
    }

    @Test
    public void testStreamId() {
        // Exercise & Verify
        assertEquals(streamId, target.streamId());
    }

    @Test
    public void toDomainEvent() {
        // Exercise & Verify
        assertEquals(event, target.toDomainEvent());
    }

    @Test
    public void testHashCode() {
        // Exercice & Verify
        assertEquals(target.hashCode(), target.hashCode());
        assertEquals(target.hashCode(), same.hashCode());
    }

    @Test
    public void testHashCode_different() {
        // Exercice & Verify
        assertNotEquals(target.hashCode(), different1.hashCode());
        assertNotEquals(target.hashCode(), different2.hashCode());
    }

    @Test
    public void testEquals() {
        // Exercice & Verify
        assertTrue(target.equals(target));
        assertTrue(target.equals(same));
    }

    @Test
    public void testEquals_different() {
        // Exercice & Verify
        assertFalse(target.equals(different1));
        assertFalse(target.equals(different2));
    }

    @Test
    public void testToString() {
        // Exercise
        String str = target.toString();

        // Verify
        assertTrue(str.contains(Long.toString(EVENT_ID)));
        assertTrue(str.contains(now.toString()));
        assertTrue(str.contains(EVENT_TYPE));
        assertTrue(str.contains(Arrays.toString(eventBody)));
        assertTrue(str.contains(streamId.toString()));
    }
}
