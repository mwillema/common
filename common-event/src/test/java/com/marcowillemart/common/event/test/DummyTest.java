package com.marcowillemart.common.event.test;

import com.google.protobuf.Message;
import com.marcowillemart.common.test.Event.DummyCreated;
import com.marcowillemart.common.test.Event.DummyUpdated;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests for the Dummy class.
 *
 * @author Marco Willemart
 */
public class DummyTest {

    private static final String ID = DummyId.nextIdentity().id();
    private static final int VALUE_1 = 10;
    private static final int VALUE_2 = 20;

    private Dummy target;

    @Test
    public void testCreateDummy() {
        // Setup
        List<Message> changes = Arrays.<Message>asList(
                DummyCreated.newBuilder().setId(ID).build());

        // Exercise
        target = new Dummy(new DummyId(ID));

        // Verify
        assertEquals(ID, target.dummyId().id());
        assertEquals(ID, target.identity());
        assertEquals(0, target.version());
        assertEquals(changes, target.changes());
    }

    @Test
    public void testUpdateDummy() {
        // Setup
        List<Message> changes = Arrays.<Message>asList(
                DummyCreated.newBuilder().setId(ID).build(),
                DummyUpdated.newBuilder().setValue(VALUE_1).build());
        target = new Dummy(new DummyId(ID));

        // Exercise
        target.update(VALUE_1);

        // Verify
        assertEquals(VALUE_1, target.value());
        assertEquals(0, target.version());
        assertEquals(changes, target.changes());
    }

    @Test
    public void testRehydrate() {
        // Setup
        final int version = 3;
        final List<Message> changes = Arrays.<Message>asList(
                DummyCreated.newBuilder().setId(ID).build(),
                DummyUpdated.newBuilder().setValue(VALUE_1).build(),
                DummyUpdated.newBuilder().setValue(VALUE_2).build());

        // Exercise
        target = new Dummy(ID, version, changes);

        // Verify
        assertEquals(ID, target.dummyId().id());
        assertEquals(VALUE_2, target.value());
        assertEquals(version, target.version());
        assertTrue(target.changes().isEmpty());
    }

    @Test
    public void testToString() {
        // Setup
        final String expected = "Dummy [identity=" + ID + "]";
        target = new Dummy(new DummyId(ID));

        // Exercise & Verify
        assertEquals(expected, target.toString());
    }
}
