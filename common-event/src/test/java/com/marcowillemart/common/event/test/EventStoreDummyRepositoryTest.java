package com.marcowillemart.common.event.test;

import com.google.protobuf.Message;
import com.marcowillemart.common.event.EventStoreConcurrencyException;
import static com.marcowillemart.common.event.test.Tests.*;
import com.marcowillemart.common.infrastructure.persistence.event.InMemoryEventStore;
import com.marcowillemart.common.test.Event.DummyUpdated;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for the EventStoreDummyRepository class.
 *
 * @author Marco Willemart
 */
public class EventStoreDummyRepositoryTest {

    private static final String ID = DummyId.nextIdentity().id();
    private static final int VALUE_1 = 10;
    private static final int VALUE_2 = 20;

    private EventStoreDummyRepository target;

    @Before
    public void setUp() {
        target = new EventStoreDummyRepository(new InMemoryEventStore());
    }

    @Test
    public void testSave_newAggregate_oneChange() {
        // Setup
        Dummy dummy = new Dummy(new DummyId(ID));

        // Exercise
        target.save(dummy);

        // Verify
        assertTrue(target.exists(new DummyId(ID)));
        assertDummyEquals(dummy, target.find(new DummyId(ID)));
    }

    @Test
    public void testSave_newAggregate_manyChanges() {
        // Setup
        Dummy dummy = new Dummy(new DummyId(ID));
        dummy.update(VALUE_1);

        // Exercise
        target.save(dummy);

        // Verify
        assertTrue(target.exists(new DummyId(ID)));
        assertDummyEquals(dummy, target.find(new DummyId(ID)));
    }

    @Test
    public void testSave_updateAggregate_zeroChange() {
        // Setup
        target.save(new Dummy(new DummyId(ID)));
        Dummy dummy = target.find(new DummyId(ID));

        // Exercise
        target.save(dummy);

        // Verify
        assertTrue(target.exists(new DummyId(ID)));
        assertDummyEquals(dummy, target.find(new DummyId(ID)));
    }

    @Test
    public void testSave_updateAggregate_oneChange() {
        // Setup
        target.save(new Dummy(new DummyId(ID)));
        Dummy dummy = target.find(new DummyId(ID));
        dummy.update(VALUE_1);

        // Exercise
        target.save(dummy);

        // Verify
        assertTrue(target.exists(new DummyId(ID)));
        assertDummyEquals(dummy, target.find(new DummyId(ID)));
    }

    @Test
    public void testSave_updateAggregate_manyChanges() {
        // Setup
        target.save(new Dummy(new DummyId(ID)));
        Dummy dummy = target.find(new DummyId(ID));
        dummy.update(VALUE_1);
        dummy.update(VALUE_2);

        // Exercise
        target.save(dummy);

        // Verify
        assertTrue(target.exists(new DummyId(ID)));
        assertDummyEquals(dummy, target.find(new DummyId(ID)));
    }

    @Test
    public void testSave_ConcurrencyException_oneEvent() {
        // Setup
        final List<Message> actualEvents =
                Arrays.<Message>asList(
                        DummyUpdated.newBuilder().setValue(VALUE_1).build());
        target.save(new Dummy(new DummyId(ID)));
        Dummy dummy = target.find(new DummyId(ID));
        {
            Dummy other = target.find(new DummyId(ID));
            other.update(VALUE_1);
            target.save(other);
        }
        dummy.update(VALUE_2);

        // Exercise & Verify
        try {
            target.save(dummy);
            fail();
        } catch (EventStoreConcurrencyException ex) {
            assertEquals(ID, ex.name());
            assertEquals(1, ex.expectedVersion());
            assertEquals(2, ex.actualVersion());
            assertEquals(actualEvents, ex.actualEvents());
        }
    }

    @Test
    public void testSave_ConcurrencyException_manyEvents() {
        // Setup
        final List<Message> actualEvents =
                Arrays.<Message>asList(
                        DummyUpdated.newBuilder().setValue(VALUE_1).build(),
                        DummyUpdated.newBuilder().setValue(VALUE_2).build());
        target.save(new Dummy(new DummyId(ID)));
        Dummy dummy = target.find(new DummyId(ID));
        {
            Dummy other = target.find(new DummyId(ID));
            other.update(VALUE_1);
            other.update(VALUE_2);
            target.save(other);
        }
        dummy.update(VALUE_1);

        // Exercise & Verify
        try {
            target.save(dummy);
            fail();
        } catch (EventStoreConcurrencyException ex) {
            assertEquals(ID, ex.name());
            assertEquals(1, ex.expectedVersion());
            assertEquals(3, ex.actualVersion());
            assertEquals(actualEvents, ex.actualEvents());
        }
    }

    @Test
    public void testFind_oneEvent() {
        // Setup
        Dummy expected = new Dummy(new DummyId(ID));
        target.save(expected);

        // Exercise
        Dummy actual = target.find(new DummyId(ID));

        // Verify
        assertDummyEquals(expected, actual);
        assertEquals(1, actual.version());
        assertTrue(actual.changes().isEmpty());
    }

    @Test
    public void testFind_manyEvents() {
        // Setup
        Dummy expected = new Dummy(new DummyId(ID));
        expected.update(VALUE_1);
        expected.update(VALUE_2);
        target.save(expected);

        // Exercise
        Dummy actual = target.find(new DummyId(ID));

        // Verify
        assertDummyEquals(expected, actual);
        assertEquals(3, actual.version());
        assertTrue(actual.changes().isEmpty());
    }

    @Test
    public void testExists_false() {
        // Exercise & Verify
        assertFalse(target.exists(new DummyId(ID)));
        assertFalse(target.exists(DummyId.nextIdentity()));
    }
}
