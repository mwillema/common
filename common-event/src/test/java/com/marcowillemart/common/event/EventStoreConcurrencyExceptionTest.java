package com.marcowillemart.common.event;

import com.google.protobuf.Message;
import com.marcowillemart.common.test.Event.DummyCreated;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests for the EventStoreConcurrencyException class.
 *
 * @author Marco Willemart
 */
public class EventStoreConcurrencyExceptionTest {

    private EventStoreConcurrencyException target;

    @Test
    public void testConstructor() {
        // Setup
        final String name = "SCL-C-2017-02-28-123456789ABC";
        final int expectedVersion = 3;
        final int actualVersion = 4;
        final List<Message> actualEvents =
                Arrays.<Message>asList(
                        DummyCreated.getDefaultInstance());
        final String message =
                "Expected v"
                + expectedVersion
                + " but found v"
                + actualVersion
                + " in stream '"
                + name
                + "'";

        // Exercise
        target = new EventStoreConcurrencyException(
                name,
                expectedVersion,
                actualVersion,
                actualEvents);

        // Verify
        assertEquals(message, target.getMessage());
        assertEquals(name, target.name());
        assertEquals(expectedVersion, target.expectedVersion());
        assertEquals(actualVersion, target.actualVersion());
        assertEquals(actualEvents, target.actualEvents());
    }
}
