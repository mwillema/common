package com.marcowillemart.common.event.test;

import com.google.protobuf.Message;
import com.marcowillemart.common.event.EventStreamId;
import com.marcowillemart.common.event.Snapshot;
import com.marcowillemart.common.event.StoredEvent;
import com.marcowillemart.common.test.Dto;
import static com.marcowillemart.common.test.Tests.*;
import com.marcowillemart.common.util.DataTable;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import static org.junit.Assert.*;

/**
 * Tests is a utility class that provides useful methods for testing purposes.
 *
 * @author Marco Willemart
 * @author Marco Willemart
 */
public final class Tests {

    private static final AtomicInteger LAST_ID = new AtomicInteger();

    private static final LocalDateTime DATE_1 = LocalDateTime.now();
    private static final LocalDateTime DATE_2 = DATE_1.plusSeconds(30);
    private static final LocalDateTime DATE_3 = DATE_2.plusMinutes(5);
    private static final LocalDateTime DATE_4 = DATE_3.plusHours(1);

    /** this cannot be initialized */
    private Tests() {
        throw new AssertionError();
    }

    ////////////////////
    // CREATION METHODS
    ////////////////////

    public static EventStreamId createAnonymousEventStreamId() {
        return new EventStreamId(
                createAnonymousName(),
                createPositiveRandomNumber());
    }

    public static Snapshot createAnonymousSnapshot(String identity) {
        return new Snapshot(
                identity,
                createPositiveRandomNumber(),
                createAnonymousMessage());
    }

    public static StoredEvent createAnonymousStoredEvent() {
        Message event = createAnonymousDtoSample();

        return new StoredEvent(
                LAST_ID.incrementAndGet(),
                event.getClass().getName(),
                event.toByteArray(),
                LocalDateTime.now());
    }

    public static StoredEvent createAnonymousStoredEvent_withStreamId() {
        Message event = createAnonymousDtoSample();

        return new StoredEvent(
                LAST_ID.incrementAndGet(),
                event.getClass().getName(),
                event.toByteArray(),
                LocalDateTime.now(),
                createAnonymousEventStreamId());
    }

    public static List<DataTable> createDataTables() {
        // Overview Table
        List<String> columns0 =
                Arrays.asList(
                        "eventId", "eventType", "occurredOn",
                        "streamName", "streamVersion");

        DataTable table0 = new DataTable("Overview", columns0);

        table0.addRow(
                Arrays.asList(
                        "1", "Sample", DATE_1.toString(),
                        "SampleStream", "1"));
        table0.addRow(
                Arrays.asList("2", "OtherSample", DATE_2.toString(), "", ""));
        table0.addRow(
                Arrays.asList(
                        "3", "Sample", DATE_3.toString(),
                        "SampleStream", "2"));
        table0.addRow(
                Arrays.asList("4", "OtherSample", DATE_4.toString(), "", ""));

        // Sample Table
        List<String> columns1 =
                Arrays.asList("eventId", "occurredOn", "id", "name");
        DataTable table1 = new DataTable("Sample", columns1);
        table1.addRow(Arrays.asList("1", DATE_1.toString(), "1", "Toto"));
        table1.addRow(Arrays.asList("3", DATE_3.toString(), "2", "Titi"));

        // OtherSample Table
        List<String> columns2 =
                Arrays.asList("eventId", "occurredOn", "id", "name", "email");
        DataTable table2 = new DataTable("OtherSample", columns2);
        table2.addRow(
                Arrays.asList(
                        "2", DATE_2.toString(),
                        "1", "Tata", "tata@titi.com"));
        table2.addRow(
                Arrays.asList(
                        "4", DATE_4.toString(),
                        "2", "Tutu", "tutu@titi.com"));

        return Arrays.asList(table0, table1, table2);
    }

    public static List<StoredEvent> createStoredEvents() {
        List<StoredEvent> storedEvents = new LinkedList<>();

        Dto.Sample event1 = Dto.Sample.newBuilder()
                .setId(1)
                .setName("Toto")
                .build();

        Dto.Sample event3 = Dto.Sample.newBuilder()
                .setId(2)
                .setName("Titi")
                .build();

        Dto.OtherSample event2 = Dto.OtherSample.newBuilder()
                .setId(1)
                .setName("Tata")
                .setEmail("tata@titi.com")
                .build();

        Dto.OtherSample event4 = Dto.OtherSample.newBuilder()
                .setId(2)
                .setName("Tutu")
                .setEmail("tutu@titi.com")
                .build();

        String sampleType = event1.getClass().getName();
        String otherSampleType = event2.getClass().getName();

        storedEvents.add(
                new StoredEvent(
                        1,
                        sampleType,
                        event1.toByteArray(),
                        DATE_1,
                        new EventStreamId("SampleStream", 1)));

        storedEvents.add(
                new StoredEvent(
                        2,
                        otherSampleType,
                        event2.toByteArray(),
                        DATE_2));

        storedEvents.add(
                new StoredEvent(
                        3,
                        sampleType,
                        event3.toByteArray(),
                        DATE_3,
                        new EventStreamId("SampleStream", 2)));

        storedEvents.add(
                new StoredEvent(
                        4,
                        otherSampleType,
                        event4.toByteArray(),
                        DATE_4));

        return storedEvents;
    }

    ////////////////////
    // CUSTOM ASSERTIONS
    ////////////////////

    public static void assertDataTablesEquals(List<DataTable> expected,
            List<DataTable> actual) {
        assertEquals(expected.size(), actual.size());

        int size = expected.size();
        for (int i = 0; i < size; i++) {
            assertDataTableEquals(expected.get(i), actual.get(i));
        }
    }

    public static void assertDataTableEquals(
            DataTable expected,
            DataTable actual) {

        assertEquals(expected.name(), actual.name());

        Iterator<List<?>> iteratorExp = expected.iterator();
        Iterator<List<?>> iteratorAct = actual.iterator();

        while (iteratorExp.hasNext() && iteratorAct.hasNext()) {
            assertEquals(iteratorExp.next(), iteratorAct.next());
        }

        assertFalse(iteratorExp.hasNext());
        assertFalse(iteratorAct.hasNext());
    }

    public static void assertDummyEquals(Dummy expected, Dummy actual) {
        assertEquals("id", expected.dummyId(), actual.dummyId());
        assertEquals("value", expected.value(), actual.value());
        int expectedVersion = expected.version() + expected.changes().size();
        int acutalVersion = actual.version() + actual.changes().size();
        assertEquals("version", expectedVersion, acutalVersion);
    }
}
