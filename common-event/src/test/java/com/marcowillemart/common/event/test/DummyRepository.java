package com.marcowillemart.common.event.test;

import com.marcowillemart.common.domain.Repository;

/**
 * DummyRepository represents a mutable repository for dummy aggregates.
 *
 * @author Marco Willemart
 */
public interface DummyRepository extends Repository<Dummy, DummyId> {
}
