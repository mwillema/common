package com.marcowillemart.common.event;

import static com.marcowillemart.common.event.test.Tests.*;
import com.marcowillemart.common.util.DataTable;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for the StoredEventExtractor class.
 *
 * @author Marco Willemart
 */
public class StoredEventExtractorTest {

    private StoredEventExtractor target;

    @Before
    public void setUp() {
        target = new StoredEventExtractor();
    }

    @Test
    public void testExtract() {
        // Setup
        List<StoredEvent> events = createStoredEvents();
        List<DataTable> expected = createDataTables();

        // Exercise
        List<DataTable> actual = target.extract(events);

        // Verify
        assertDataTablesEquals(expected, actual);
    }
}
