package com.marcowillemart.common.event.test;

import com.marcowillemart.common.event.EventStore;
import com.marcowillemart.common.event.SnapshotEventStoreRepository;
import com.marcowillemart.common.event.SnapshotRepository;
import org.springframework.stereotype.Repository;

/**
 * Event store implementation with snapshotting of the DummyRepository
 * interface.
 *
 * @author Marco Willemart
 */
@Repository
public class SnapshotEventStoreDummyRepository
        extends SnapshotEventStoreRepository<Dummy, DummyId>
        implements DummyRepository {

    public SnapshotEventStoreDummyRepository(
            EventStore eventStore,
            SnapshotRepository snapshotRepository) {

        super(eventStore, snapshotRepository, Dummy.class, 2);
    }
}
