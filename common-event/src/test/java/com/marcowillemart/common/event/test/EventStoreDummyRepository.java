package com.marcowillemart.common.event.test;

import com.marcowillemart.common.event.EventStore;
import com.marcowillemart.common.event.EventStoreRepository;
import org.springframework.stereotype.Repository;

/**
 * Event store implementation of the DummyRepository interface.
 *
 * @author Marco Willemart
 */
@Repository
public class EventStoreDummyRepository
        extends EventStoreRepository<Dummy, DummyId>
        implements DummyRepository {

    public EventStoreDummyRepository(EventStore eventStore) {
        super(eventStore, Dummy.class);
    }
}
