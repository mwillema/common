package com.marcowillemart.common.event;

import static com.marcowillemart.common.test.Tests.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for the EventStreamId class.
 *
 * @author Marco Willemart
 */
public class EventStreamIdTest {

    private String name;
    private String nameSegment1;
    private String nameSegment2;
    private int version;

    private EventStreamId target;
    private EventStreamId same;
    private EventStreamId other1;
    private EventStreamId other2;

    @Before
    public void setUp() {
        nameSegment1 = createAnonymousName();
        nameSegment2 = createAnonymousName();
        name = nameSegment1 + ":" + nameSegment2;
        version = createRandomNumber() + 1;

        target = new EventStreamId(name, version);
        same = new EventStreamId(nameSegment1, nameSegment2, version);
        other1 = new EventStreamId(nameSegment1, version);
        other2 = new EventStreamId(name, createRandomNumber() + 1);
    }

    @Test
    public void testConstructor() {
        // Exercise
        EventStreamId expected = new EventStreamId(name, version);

        // Verify
        assertEquals(name, expected.name());
        assertEquals(version, expected.version());
    }

    @Test
    public void testConstructor_versionOne() {
        // Exercise
        EventStreamId actual = new EventStreamId(name);

        // Verify
        EventStreamId expected = new EventStreamId(name, 1);
        assertEquals(expected, actual);
    }

    @Test
    public void testConstructor_nameSegments() {
        // Exercise
        EventStreamId actual =
                new EventStreamId(nameSegment1, nameSegment2, version);

        // Verify
        EventStreamId expected = new EventStreamId(name, version);
        assertEquals(expected, actual);
    }

    @Test
    public void testConstructor_nameSegmentsAndVersionOne() {
        // Exercise
        EventStreamId actual = new EventStreamId(nameSegment1, nameSegment2);

        // Verify
        EventStreamId expected =
                new EventStreamId(nameSegment1, nameSegment2, 1);
        assertEquals(expected, actual);
    }

    @Test
    public void testWithVersion() {
        // Setup
        final int newVersion = 3;

        // Exercise
        EventStreamId expected = target.withVersion(newVersion);

        // Verify
        EventStreamId acutal = new EventStreamId(target.name(), newVersion);
        assertEquals(expected, acutal);
    }

    @Test
    public void testEquals_same() {
        assertTrue(target.equals(target));
        assertTrue(target.equals(same));
    }

    @Test
    public void testEquals_different() {
        assertFalse(target.equals(other1));
        assertFalse(target.equals(other2));
    }

    @Test
    public void testHashCode_same() {
        assertEquals(target.hashCode(), target.hashCode());
        assertEquals(same.hashCode(), target.hashCode());
    }

    @Test
    public void testHashCode_different() {
        assertNotEquals(other1.hashCode(), target.hashCode());
        assertNotEquals(other2.hashCode(), target.hashCode());
    }

    @Test
    public void testToString() {
        // Exercise
        String actual = target.toString();

        // Verify
        assertTrue(actual.contains(target.name()));
        assertTrue(actual.contains(Integer.toString(target.version())));
    }
}
