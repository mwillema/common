package com.marcowillemart.common.event.test;

import static com.marcowillemart.common.event.test.Tests.*;
import com.marcowillemart.common.infrastructure.persistence.event.InMemoryEventStore;
import com.marcowillemart.common.infrastructure.persistence.event.InMemorySnapshotRepository;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for the SnapshotEventStoreDummyRepository class.
 *
 * @author Marco Willemart
 */
public class SnapshotEventStoreDummyRepositoryTest {

    private static final String ID = DummyId.nextIdentity().id();
    private static final int VALUE_1 = 10;
    private static final int VALUE_2 = 20;
    private static final int VALUE_3 = 30;
    private static final int VALUE_4 = 40;

    private SnapshotEventStoreDummyRepository target;

    @Before
    public void setUp() {
        target = new SnapshotEventStoreDummyRepository(
                new InMemoryEventStore(),
                new InMemorySnapshotRepository());
    }

    @Test
    public void testSave_newAggregate() {
        // Setup
        Dummy dummy = new Dummy(new DummyId(ID));

        // Exercise
        target.save(dummy);

        // Verify
        assertTrue(target.exists(new DummyId(ID)));
        assertDummyEquals(dummy, target.find(new DummyId(ID)));
    }

    @Test
    public void testSave_newAggregate_newSnapshot() {
        // Setup
        Dummy dummy = new Dummy(new DummyId(ID));
        dummy.update(VALUE_1);

        // Exercise
        target.save(dummy);

        // Verify
        assertTrue(target.exists(new DummyId(ID)));
        assertDummyEquals(dummy, target.find(new DummyId(ID)));
    }

    @Test
    public void testSave_updateAggregate() {
        // Setup
        target.save(new Dummy(new DummyId(ID)));
        Dummy dummy = target.find(new DummyId(ID));
        dummy.update(VALUE_1);
        dummy.update(VALUE_2);

        // Exercise
        target.save(dummy);

        // Verify
        assertTrue(target.exists(new DummyId(ID)));
        assertDummyEquals(dummy, target.find(new DummyId(ID)));
    }

    @Test
    public void testSave_updateAggregate_newSnapshot() {
        // Setup
        target.save(new Dummy(new DummyId(ID)));
        Dummy dummy = target.find(new DummyId(ID));
        dummy.update(VALUE_1);
        dummy.update(VALUE_2);
        dummy.update(VALUE_3);

        // Exercise
        target.save(dummy);

        // Verify
        assertTrue(target.exists(new DummyId(ID)));
        assertDummyEquals(dummy, target.find(new DummyId(ID)));
    }

    @Test
    public void testSave_updateAggregate_updateSnapshot() {
        // Setup
        {
            Dummy dummy = new Dummy(new DummyId(ID));
            dummy.update(VALUE_1);
            target.save(dummy);
        }

        Dummy dummy = target.find(new DummyId(ID));
        dummy.update(VALUE_2);
        dummy.update(VALUE_3);

        // Exercise
        target.save(dummy);

        // Verify
        assertTrue(target.exists(new DummyId(ID)));
        assertDummyEquals(dummy, target.find(new DummyId(ID)));
    }

    @Test
    public void testSave_updateAggregate_zeroChange_updateSnapshot() {
        // Setup
        {
            Dummy dummy = new Dummy(new DummyId(ID));
            dummy.update(VALUE_1);
            target.save(dummy);
        }

        Dummy dummy = target.find(new DummyId(ID));

        // Exercise
        target.save(dummy);

        // Verify
        assertTrue(target.exists(new DummyId(ID)));
        assertDummyEquals(dummy, target.find(new DummyId(ID)));
    }

    @Test
    public void testFind_noSnapshot() {
        // Setup
        Dummy expected = new Dummy(new DummyId(ID));
        target.save(expected);

        // Exercise
        Dummy actual = target.find(new DummyId(ID));

        // Verify
        assertDummyEquals(expected, actual);
    }

    @Test
    public void testFind_snapshotIsLastState() {
        // Setup
        Dummy expected = new Dummy(new DummyId(ID));
        expected.update(VALUE_1);
        expected.update(VALUE_2);
        expected.update(VALUE_3);
        target.save(expected);

        // Exercise
        Dummy actual = target.find(new DummyId(ID));

        // Verify
        assertDummyEquals(expected, actual);
    }

    @Test
    public void testFind_oneEventReplayedAfterSnapshot() {
        // Setup
        Dummy expected = new Dummy(new DummyId(ID));
        expected.update(VALUE_1);
        target.save(expected);

        expected = target.find(new DummyId(ID));
        expected.update(VALUE_2);
        target.save(expected);

        // Exercise
        Dummy actual = target.find(new DummyId(ID));

        // Verify
        assertDummyEquals(expected, actual);
    }

    @Test
    public void testFind_manyEventsReplayedAfterSnapshot() {
        // Setup
        Dummy expected = new Dummy(new DummyId(ID));
        expected.update(VALUE_1);
        target.save(expected);

        expected = target.find(new DummyId(ID));
        expected.update(VALUE_2);
        expected.update(VALUE_3);
        expected.update(VALUE_4);
        target.save(expected);

        // Exercise
        Dummy actual = target.find(new DummyId(ID));

        // Verify
        assertDummyEquals(expected, actual);
    }
}
