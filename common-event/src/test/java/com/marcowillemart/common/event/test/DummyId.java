package com.marcowillemart.common.event.test;

import com.marcowillemart.common.domain.IdentityGenerator;
import com.marcowillemart.common.domain.StructuredId;
import com.marcowillemart.common.util.Assert;

/**
 * DummyId is a value object representing the unique identity of a dummy
 * aggregate.
 *
 * @author Marco Willemart
 */
public final class DummyId extends StructuredId {

    private static final String KEY = "SCL";
    private static final char CODE = 'D';

    /**
     * @requires id != null && id.length > 0
     * @effects Makes this be a new DummyId i with i.id = id
     */
    public DummyId(String id) {
        super(id);

        Assert.equals(KEY, key());
        Assert.equals(CODE, code());
    }

    public static DummyId nextIdentity() {
        return new DummyId(
                IdentityGenerator.defaultInstance().newIdentity(KEY, CODE));
    }
}
