package com.marcowillemart.common.infrastructure.persistence.event;

import com.marcowillemart.common.event.Snapshot;
import com.marcowillemart.common.event.SnapshotRepository;
import static com.marcowillemart.common.event.test.Tests.*;
import static com.marcowillemart.common.test.Tests.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class InMemorySnapshotRepositoryTest {

private static final String ID = createUUID();

    private SnapshotRepository target;

    @Before
    public void setUp() {
        target = new InMemorySnapshotRepository();
    }

    @Test
    public void testSave_insert() {
        // Setup
        Snapshot snapshot = createAnonymousSnapshot(ID);

        // Exercise
        target.save(snapshot);

        // Verify
        assertTrue(target.exists(ID));
        assertEquals(snapshot, target.find(ID));
    }

    @Test
    public void testSave_update() {
        // Setup
        target.save(createAnonymousSnapshot(ID));
        Snapshot snapshot = createAnonymousSnapshot(ID);

        // Exercise
        target.save(snapshot);

        // Verify
        assertTrue(target.exists(ID));
        assertEquals(snapshot, target.find(ID));
    }

    @Test
    public void testExists_false() {
        // Exercise & Verify
        assertFalse(target.exists(ID));
    }
}
