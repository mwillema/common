package com.marcowillemart.common.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Lists is a utility class that provides useful methods for manipulating
 * {@link java.util.List} instances.
 *
 * @author Marco Willemart
 */
public final class Lists {

    /** this cannot be instantiated */
    private Lists() {
        throw new AssertionError();
    }

    /**
     * @requires for all e in elements, e != null
     * @return a new ArrayList containing the given elements in the specified
     *         order.
     */
    @SafeVarargs
    public static <E> ArrayList<E> newArrayList(E... elements) {
        ArrayList<E> list = new ArrayList<>(elements.length);
        Collections.addAll(list, elements);
        return list;
    }

    /**
     * @requires for all e in elements, e != null
     * @return a new LinkedList containing the given elements in the specified
     *         order.
     */
    @SafeVarargs
    public static <E> LinkedList<E> newLinkedList(E... elements) {
        LinkedList<E> list = new LinkedList<>();
        Collections.addAll(list, elements);
        return list;
    }

    /**
     * @requires list != null && list.size > 0
     * @return the first element of the given list.
     */
    public static <E> E first(List<E> list) {
        Assert.notEmpty(list);

        return list.get(0);
    }

    /**
     * @requires list != null && list.size > 0
     * @return the last element of the given list.
     */
    public static <E> E last(List<E> list) {
        Assert.notEmpty(list);

        return list.get(list.size() - 1);
    }

    /**
     * @requires generator != null
     * @modifies generator
     * @effects Consumes generator until it has no next element to produce.
     * @return a new list initialized with the elements produced by generator.
     */
    public static <E> List<E> toList(Iterator<E> generator) {
        Assert.notNull(generator);

        List<E> set = new LinkedList<>();

        while (generator.hasNext()) {
            set.add(generator.next());
        }

        return set;
    }

    /**
     * @requires list != null && from in [1..list.size] && to in [1..list.size]
     * @modifies list
     * @effects Moves the element at position 'from' to position 'to' in 'list'
     * @return the moved element
     */
    public static <T> T move(List<T> list, int from, int to) {
        T element = list.remove(from - 1);
        list.add(to - 1, element);

        return element;
    }
}
