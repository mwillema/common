package com.marcowillemart.common.util;

import java.io.*;
import java.nio.charset.Charset;
import java.sql.Blob;
import java.sql.SQLException;
import javax.sql.rowset.serial.SerialBlob;

/**
 * IOUtils is a utility class that provides useful methods for working with
 * IO streams.
 *
 * @author Marco Willemart
 */
public final class IOUtils {

    /** The default buffer size to use. */
    private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;

    private static final int EOF = -1;

    /** this cannot be instantiated */
    private IOUtils() {
        throw new AssertionError();
    }

    /**
     * @requires object != null
     * @return a newly allocated byte array containing the serialization of the
     *         given object.
     */
    public static byte[] serialize(Object object) {
        Assert.notNull(object);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try (ObjectOutputStream out = new ObjectOutputStream(bos)) {
            out.writeObject(object);
            return bos.toByteArray();
        } catch (IOException ex) {
            throw new FailureException("IOUtils.serialize", ex);
        }
    }

    /**
     * @requires buffer != null && buffer contains a valid serialization of an
     *           object
     * @return the object deserialized from the given byte buffer.
     */
    public static Object deserialize(byte[] buffer) {
        Assert.notNull(buffer);

        ByteArrayInputStream bis = new ByteArrayInputStream(buffer);
        return deserialize(bis);
    }

    /**
     * @requires stream != null && stream is a valid serialization of an object
     * @return the object deserialized from the given input stream.
     */
    public static Object deserialize(InputStream stream) {
        Assert.notNull(stream);

        try (ObjectInputStream in = new ObjectInputStream(stream)) {
            return in.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            throw new FailureException("IOUtils.deserialize", ex);
        }
    }

    /**
     * @requires buffer != null
     * @return a blob whose value is set to the given buffer.
     */
    public static Blob toBlob(byte[] buffer) {
        Assert.notNull(buffer);

        try {
            return new SerialBlob(buffer);
        } catch (SQLException ex) {
            throw new FailureException("IOUtils.toBlob", ex);
        }
    }

    /**
     * @requires blob != null
     * @return the contents of the given blob as a byte array.
     */
    public static byte[] toByteArray(Blob blob) {
        Assert.notNull(blob);

        try {
            return toByteArray(blob.getBinaryStream());
        } catch (SQLException ex) {
            throw new FailureException("IOUtils.toByteArray(Blob)", ex);
        }
    }

    /**
     * @requires input != null
     * @return the contents of the given input stream as a byte array.
     */
    public static byte[] toByteArray(InputStream input) {
        Assert.notNull(input);

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        copy(input, output);
        return output.toByteArray();
    }

    /**
     * @requires input != null
     * @return the contents of the given input stream as a string.
     */
    public static String toString(InputStream input) {
        Assert.notNull(input);

        StringWriter writer = new StringWriter();
        copy(input, writer);
        return writer.toString();
    }

    /**
     * @requires input != null && output != null
     * @modifies input, output
     * @effects Copies the bytes from the given input stream to the given output
     *          stream.
     * @return the number of bytes copied, of -1 if > Integer.MAX_VALUE.
     */
    public static int copy(InputStream input, OutputStream output) {
        long count = copyLarge(input, output);

        if (count > Integer.MAX_VALUE) {
            return -1;
        }

        return (int) count;
    }

    /**
     * @requires input != null && file != null && file is not a directory and
     *           its parent exists
     * @modifies input, file
     * @effects Copies the given input stream into the given file.
     */
    public static void copy(InputStream input, File file) {
        Assert.notNull(input);
        Assert.notNull(file);
        Assert.isFalse(file.isDirectory());
        Assert.isTrue(file.getParentFile().exists());

        try (OutputStream output =
                new BufferedOutputStream(
                        new FileOutputStream(file))) {
            copy(input, output);
        } catch (IOException ex) {
            throw new FailureException("IOUtils.copy(InputStream,File)", ex);
        }
    }

    /**
     * @requires data != null && file != null &&
     *           file is not a directory and its parent exists
     * @modifies file
     * @effects Copies the bytes from data to the given file.
     */
    public static void copy(byte[] data, File file) {
        Assert.notNull(data);

        copy(new ByteArrayInputStream(data), file);
    }

    /**
     * @requires input != null && output != null
     * @modifies input, output
     * @effects Copies the bytes from the given input stream to chars on the
     *          given writer using the default character encoding of the
     *          platform.
     */
    public static void copy(InputStream input, Writer output) {
        Assert.notNull(input);
        Assert.notNull(output);

        InputStreamReader in =
                new InputStreamReader(input, Charset.defaultCharset());

        copy(in, output);
    }

    /**
     * @requires input != null && output != null
     * @modifies input, output
     * @effects Copies the chars from the given reader to the given writer.
     * @return the number of bytes copied, of -1 if > Integer.MAX_VALUE.
     */
    public static int copy(Reader input, Writer output) {
        long count = copyLarge(input, output);

        if (count > Integer.MAX_VALUE) {
            return -1;
        }

        return (int) count;
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    /**
     * @requires input != null && output != null
     * @modifies input, output
     * @effects Copies the bytes from the given input stream (may be large,
     *          i.e., over 2GB) to the given output stream.
     * @return the number of bytes copied.
     */
    private static long copyLarge(InputStream input, OutputStream output) {
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        long count = 0;
        int length = 0;

        try {
            while (EOF != (length = input.read(buffer))) {
                output.write(buffer, 0, length);
                count += length;
            }
        } catch (IOException ex) {
            throw new FailureException(
                    "IOUtils.copyLarge(InputStream,OutputStream)", ex);
        }

        return count;
    }

    /**
     * @requires input != null && output != null
     * @modifies input, output
     * @effects Copies the chars from the given reader (may be large, i.e.,
     *          over 2GB) to the given writer.
     * @return the number of bytes copied.
     */
    private static long copyLarge(Reader input, Writer output) {
        char[] buffer = new char[DEFAULT_BUFFER_SIZE];
        long count = 0;
        int length = 0;

        try {
            while (EOF != (length = input.read(buffer))) {
                output.write(buffer, 0, length);
                count += length;
            }
        } catch (IOException ex) {
            throw new FailureException("IOUtils.copyLarge(Reader,Writer)", ex);
        }

        return count;
    }
}
