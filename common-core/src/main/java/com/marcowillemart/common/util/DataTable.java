package com.marcowillemart.common.util;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * DataTable represents a mutable data table with columns and rows.
 *
 * @specfield name : String              // The name of the table.
 * @specfield columnNames : List<String> // The column names.
 * @specfield rows : List<List<Object>>  // The rows of data.
 *
 * @invariant name not empty
 * @invariant columnNames.size > 0
 * @invariant for all r in rows, r.size == columnNames.size
 *
 * @author Marco Willemart
 */
public final class DataTable implements Iterable<List<?>> {

    private final String name;
    private final List<String> columnNames;
    private final List<List<?>> rows;

    /*
     * Representation Invariant:
     *   name != null
     *   name.length > 0
     *   columnNames != null
     *   rows != null
     *   columnNames.size > 0
     *   for all c in columnNames, c != null
     *   for all r in rows, r != null && r.size == columnNames.size
     */

    /**
     * @effects Asserts the rep invariant holds for this.
     */
    private void checkRep() {
        Assert.notEmpty(name);
        Assert.notNull(columnNames);
        Assert.notNull(rows);
        Assert.isFalse(columnNames.isEmpty());
        Assert.noNullElement(columnNames);
        for (List<?> row : rows) {
            Assert.notNull(row);
            Assert.equals(columnNames.size(), row.size());
        }
    }

    /**
     * @requires name not empty, columnNames not empty and no null element
     *           in columnNames
     * @effects Makes this be a new data table t with t.name = name,
     *          t.columnNames = columnNames and t.rows = [].
     */
    public DataTable(String name, List<String> columnNames) {
        Assert.notEmpty(name);
        Assert.notEmpty(columnNames);

        this.name = name;
        this.columnNames = new LinkedList<>(columnNames);
        this.rows = new LinkedList<>();

        checkRep();
    }

    /**
     * @return this.name
     */
    public String name()  {
        return this.name;
    }

    /**
     * @return this.columnNames.size()
     */
    public int columnSize() {
        return columnNames.size();
    }

    /**
     * @return this.columnNames
     */
    public List<String> columnNames() {
        return Collections.unmodifiableList(columnNames);
    }

    /**
     * @requires row not null && row not empty &&
     *           row.size = this.columnNames.size
     * @modifies this.rows
     * @effects Adds 'row' to this.rows
     */
    public void addRow(List<?> row) {
        Assert.notEmpty(row);
        Assert.equals(columnSize(), row.size());

        rows.add(new LinkedList<>(row));

        checkRep();
    }

    @Override
    public Iterator<List<?>> iterator() {
        return new RowGenerator();
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    /**
     * RowGenerator represents a mutable generator for reading the rows of a
     * data table.
     */
    private class RowGenerator implements Iterator<List<?>> {

        private final int nbRows;
        private int currentRow;

        /**
         * @effects Makes this be a new row generator.
         */
        RowGenerator() {
            this.nbRows = rows.size() + 1;
            this.currentRow = 0;
        }

        @Override
        public boolean hasNext() {
            return currentRow < nbRows;
        }

        @Override
        public List<?> next() {
            if (currentRow == 0) {
                currentRow++;
                return new LinkedList<>(columnNames);
            } else {
                int i = currentRow - 1;
                currentRow++;
                return new LinkedList<>(rows.get(i));
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("DataTable.iterator");
        }
    } // end RowGenerator
}
