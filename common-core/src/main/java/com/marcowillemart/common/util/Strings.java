package com.marcowillemart.common.util;

import java.util.Locale;

/**
 * Strings is a utility class that provides useful methods for manipulating
 * {@link String} instances.
 *
 * @author Marco Willemart
 */
public final class Strings {

    /** The empty string. */
    public static final String EMPTY = "";

    private static final char SINGLE_QUOTE = '\'';
    private static final char DOUBLE_QUOTE = '"';

    /** this cannot be instantiated */
    private Strings() {
        throw new AssertionError();
    }

   /**
     * @requires text != null
     * @return a new string without the leading and trailing single or double
     *         quotes if text.length >= 2 && text starts and ends with quotes
     *         (both single or double), else returns text.
     */
    public static String trimQuotes(String text) {
        Assert.notNull(text);

        if (text.length() >= 2 && surroundedByQuotes(text)) {
            return text.substring(1, text.length() - 1);
        }

        return text;
    }

    /**
     * @return true iff seq is not null, not empty, and only contains digits
     */
    public static boolean isNumeric(CharSequence seq) {
        if (isEmpty(seq)) {
            return false;
        }

        for (int i = 0; i < seq.length(); i++) {
            if (!Character.isDigit(seq.charAt(i))) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return true iff seq is null or empty
     */
    public static boolean isEmpty(CharSequence seq) {
        return seq == null || seq.length() == 0;
    }

    /**
     * @return bytes as a human readable string
     */
    public static String humanReadableByteCount(long bytes) {
        int unit = 1000;

        if (bytes < unit) {
            return bytes + " B";
        }

        int exp = (int) (Math.log(bytes) / Math.log(unit));
        char prefix = "kMGTPE".charAt(exp - 1);

        return String.format(
                Locale.ENGLISH,
                "%.1f %cB",
                bytes / Math.pow(unit, exp),
                prefix);
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private static boolean surroundedByQuotes(String text) {
        return surroundedBySingleQuotes(text) || surroundedByDoubleQuotes(text);
    }

    private static boolean surroundedBySingleQuotes(String text) {
        return text.indexOf(SINGLE_QUOTE) == 0
                && text.lastIndexOf(SINGLE_QUOTE) == text.length() - 1;
    }

    private static boolean surroundedByDoubleQuotes(String text) {
        return text.indexOf(DOUBLE_QUOTE) == 0
                && text.lastIndexOf(DOUBLE_QUOTE) == text.length() - 1;
    }
}
