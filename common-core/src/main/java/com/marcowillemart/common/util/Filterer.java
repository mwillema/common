package com.marcowillemart.common.util;

/**
 * Filterer defines a filter on objects of type T.
 *
 * @author Marco Willemart
 */
public interface Filterer<T> {

    /**
     * @return true if t passes the check, else returns false.
     */
    boolean check(T t);
}
