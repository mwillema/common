package com.marcowillemart.common.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * NotPossibleException is a checked exception with a message explaining
 * the problem to the user.
 *
 * @specfield message : String         // The default message
 * @specfield cause : Throwable [0-1]  // The cause
 * @specfield code : String [0-1]      // The code used to resolve the message
 * @specfield arguments : List<Object> // The arguments used to resolve the
 *                                        message
 *
 * @invariant arguments.size > 0 -> code != nil
 *
 * @author Marco Willemart
 */
public class NotPossibleException extends Exception {

    private final String code;
    private final List<Object> arguments;

    /**
     * @requires message != null
     * @effects Makes this be a new NotPossibleException e with
     *          e.message = message, e.cause = nil, e.code = nil and
     *          e.arguments = []
     */
    public NotPossibleException(String message) {
        this(message, null);
    }

    /**
     * @requires message != null
     * @effects Makes this be a new NotPossibleException  e with
     *          e.message = message, e.cause = cause, e.code = nil and
     *          e.arguments = []
     */
    public NotPossibleException(String message, Throwable cause) {
        this(message, cause, null);
    }

    /**
     * @requires message != null
     * @effects Makes this be a new NotPossibleException  e with
     *          e.message = message, e.cause = nil, e.code = code and
     *          e.arguments = arguments
     */
    public NotPossibleException(String message, String code,
            Object... arguments) {
        this(message, null, code, arguments);
    }

    /**
     * @requires message != null
     * @effects Makes this be a new NotPossibleException  e with
     *          e.message = message, e.cause = cause, e.code = code and
     *          e.arguments = arguments
     */
    public NotPossibleException(String message, Throwable cause, String code,
            Object... arguments) {
        super(message, cause);

        Assert.notNull(message);


        this.code = code;
        this.arguments = Arrays.asList(arguments);
    }

    /**
     * @return this.code or null if this.code is nil
     */
    public String getCode() {
        return code;
    }

    /**
     * @return this.arguments
     */
    public List<Object> getArguments() {
        return Collections.unmodifiableList(arguments);
    }
}
