package com.marcowillemart.common.util;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Sets is a utility class that provides useful methods for manipulating
 * {@link Set} instances.
 *
 * @author Marco Willemart
 */
public final class Sets {

    /** this cannot be instantiated */
    private Sets() {
        throw new AssertionError();
    }

    /**
     * @requires for all e in elements, e != null && e is unique
     * @return a new HashSet containing the given elements in unspecified order.
     */
    @SafeVarargs
    public static <E> HashSet<E> newHashSet(E... elements) {
        HashSet<E> set = new HashSet<>(elements.length);
        Collections.addAll(set, elements);
        return set;
    }

    /**
     * @requires for all e in elements, e != null && e is unique
     * @return a new LinkedHashSet containing the given elements in their
     *         initial order.
     */
    @SafeVarargs
    public static <E> LinkedHashSet<E> newLinkedHashSet(E... elements) {
        LinkedHashSet<E> set = new LinkedHashSet<>(elements.length);
        Collections.addAll(set, elements);
        return set;
    }

    /**
     * @requires set != null && filter != null
     * @return { e | e in set && filter.check(e) }
     */
    public static <E> Set<E> subSet(Set<E> set, Filterer<E> filter) {
        Assert.notNull(set);
        Assert.notNull(filter);

        Set<E> subSet = new LinkedHashSet<>();

        for (E e : set) {
            if (filter.check(e)) {
                subSet.add(e);
            }
        }

        return subSet;
    }

    /**
     * @requires generator != null
     * @modifies generator
     * @effects Consumes generator until it has no next element to produce.
     * @return a new set initialized with the elements produced by generator.
     */
    public static <E> Set<E> toSet(Iterator<E> generator) {
        Assert.notNull(generator);

        Set<E> set = new LinkedHashSet<>();

        while (generator.hasNext()) {
            set.add(generator.next());
        }

        return set;
    }
}
