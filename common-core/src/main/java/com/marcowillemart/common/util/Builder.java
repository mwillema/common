package com.marcowillemart.common.util;

/**
 * Builder represents a builder for objects of type T.
 *
 * See Item 2 from Effective Java for more information.
 *
 * @author Marco Willemart
 */
public interface Builder<T> {

    /**
     * @return a new built object of type T.
     */
    T build();
}
