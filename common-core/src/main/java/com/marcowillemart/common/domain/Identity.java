package com.marcowillemart.common.domain;

/**
 * Identity represents an immutable basic identity.
 *
 * @specfield id : String // The string representation of the identity.
 *
 * @invariant id.length > 0
 *
 * @author Marco Willemart
 */
public interface Identity {

    /**
     * @return this.id
     */
    String id();
}
