package com.marcowillemart.common.domain;

import com.marcowillemart.common.util.Assert;
import java.time.LocalDate;
import java.util.UUID;

/**
 * IdentityGenerator is a stateless utility class responsible for generating
 * global unique identities.
 *
 * @author Marco Willemart
 */
public class IdentityGenerator {

    private static final IdentityGenerator DEFAULT_INSTANCE =
            new IdentityGenerator();

    private static final int KEY_LENGTH = 3;
    private static final int ID_LENGTH = 25;
    private static final String SEGMENT_SEPARATOR = "-";

    /**
     * @effects Makes this be a new identity generator.
     */
    public IdentityGenerator() {
    }

    /**
     * @return a new universally unique identifier (UUID).
     */
    public String newIdentity() {
        return UUID.randomUUID().toString();
    }

    /**
     * @requires key != null && key.length = 3
     * @return a new 29-character identity matching the pattern
     *         key-code-yyyy-mm-dd-xxxxxxxxxxxx where yyyy-mm-dd is today's date
     *         and xxxxxxxxxxxx the last segment of a generated UUID.
     */
    public String newIdentity(String key, char code) {
        Assert.notNull(key);
        Assert.equals(KEY_LENGTH, key.length());

        StringBuilder builder = new StringBuilder(ID_LENGTH);
        builder.append(key);
        builder.append(SEGMENT_SEPARATOR);
        builder.append(code);
        builder.append(SEGMENT_SEPARATOR);
        builder.append(LocalDate.now().toString());
        builder.append(SEGMENT_SEPARATOR);
        String[] uuid = UUID.randomUUID().toString().split(SEGMENT_SEPARATOR);
        builder.append(uuid[uuid.length - 1]);

        return builder.toString().toUpperCase();
    }

    /**
     * @return the default instance of this.
     */
    public static IdentityGenerator defaultInstance() {
        return DEFAULT_INSTANCE;
    }
}
