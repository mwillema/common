package com.marcowillemart.common.domain;

/**
 * Repository represents a mutable repository of aggregates of type T that have
 * a global unique identity of type I.
 *
 * @author Marco Willemart
 */
public interface Repository<T, I> {

    /**
     * @requires aggregate != null
     * @modifies this
     * @effects Saves, i.e., inserts or updates, the given aggregate in this
     */
    void save(T aggregate);

    /**
     * @requires identity != null
     * @return true iff identity identifies an aggregate in this
     */
    boolean exists(I identity);

    /**
     * @requires identity != null && exists(identity)
     * @return the aggregate identified by identity in this
     */
    T find(I identity);
}
