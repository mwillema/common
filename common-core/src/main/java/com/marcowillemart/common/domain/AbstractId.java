package com.marcowillemart.common.domain;

/**
 * Abstract implementation of the Identity interface.
 *
 * @author Marco Willemart
 */
public abstract class AbstractId implements Identity {

    private final String id;

    /**
     * @requires id != null && id.length > 0
     * @effects Makes this be a new abstract identity i with i.id = id
     */
    protected AbstractId(String id) {
        this.id = id;
    }

    @Override
    public final String id() {
        return id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + getClass().hashCode();
        hash = 79 * hash + id.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractId other = (AbstractId) obj;
        return id.equals(other.id);
    }

    @Override
    public String toString() {
        return String.format("%s [id=%s]", getClass().getSimpleName(), id);
    }
}
