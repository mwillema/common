package com.marcowillemart.common.domain;

import com.marcowillemart.common.util.Assert;

/**
 * CompositeId represents an immutable identity made up of two segments.
 *
 * @author Marco Willemart
 */
public class CompositeId extends AbstractId {

    /**
     * @requires segment1 != null && segment2 != null
     * @effects Makes this be a new composite id i with
     *          i.id = segment1 + ":" + segment2
     */
    public CompositeId(Identity segment1, Identity segment2) {
        super(concat(segment1, segment2));
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    /**
     * @requires segment1 != null && segment2 != null
     * @return segment1.id + ":" + segment2.id
     */
    private static String concat(Identity segment1, Identity segment2) {
        Assert.notNull(segment1);
        Assert.notNull(segment2);

        return String.format("%s:%s", segment1.id(), segment2.id());
    }
}
