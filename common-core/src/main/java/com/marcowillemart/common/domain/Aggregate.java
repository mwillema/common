package com.marcowillemart.common.domain;

import com.marcowillemart.common.util.Assert;

/**
 * Aggregate is an aggregate base class.
 *
 * @specfield identity : String // The global unique identity of the aggregate.
 *
 * @invariant identity not empty
 *
 * @author Marco Willemart
 */
public abstract class Aggregate {

    private final String identity;

    /*
     * Abstraction Function:
     *   identity = identity
     *
     * Representation Invariant:
     *   identity != null
     *   identity.length > 0
     */

    /**
     * @requires identity not null and not empty
     * @effects Makes this be a new aggregate a with a.identity = identity
     */
    protected Aggregate(String identity) {
        Assert.notEmpty(identity);

        this.identity = identity;
    }

    /**
     * @requires identity not null
     * @effects Makes this be a new aggregate a with a.identity = identity.id
     */
    protected Aggregate(Identity identity) {
        Assert.notNull(identity);

        this.identity = identity.id();
    }

    public final String identity() {
        return identity;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + getClass().hashCode();
        hash = 83 * hash + identity().hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Aggregate other = (Aggregate) obj;
        return identity.equals(other.identity);
    }

    @Override
    public String toString() {
        return String.format("%s [identity=%s]",
                getClass().getSimpleName(),
                identity);
    }
}
