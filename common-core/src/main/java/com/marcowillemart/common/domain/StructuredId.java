package com.marcowillemart.common.domain;

import com.marcowillemart.common.util.Assert;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * StructuredId represents an immutable structured, self-describing identity.
 *
 * ID(id): id matches key-code-yyyy-mm-dd-xxxxxxxxxxxx where
 *           key is 3 word chars,
 *           code is 1 word char,
 *           yyyy-mm-dd is the creation date,
 *           xxxxxxxxxxxx is the last segment of the identity
 *
 * @invariant ID(id)
 *
 * @author Marco Willemart
 */
public abstract class StructuredId extends AbstractId {

    private static final int FIRST_INDEX = 0;

    private static final String KEY = "(\\w{3})";
    private static final String CODE = "(\\w)";
    private static final String DATE = "(\\d{4}-\\d{2}-\\d{2})";
    private static final String SEGMENT = "(\\w{12})";

    private static final Pattern ID_PATTERN = Pattern.compile(
            String.format("%s-%s-%s-%s", KEY, CODE, DATE, SEGMENT));

    private static final int KEY_GROUP = 1;
    private static final int CODE_GROUP = 2;
    private static final int DATE_GROUP = 3;

    private final Matcher matcher;

    /**
     * @requires id != null && ID(id)
     * @effects Makes this be a new identity i with i.id = id
     */
    protected StructuredId(String id) {
        super(id);

        this.matcher = ID_PATTERN.matcher(id);

        Assert.isTrue(this.matcher.matches());
    }

    /**
     * @return the key of this
     */
    public final String key() {
        return matcher.group(KEY_GROUP);
    }

    /**
     * @return the code of this
     */
    public final char code() {
        return matcher.group(CODE_GROUP).charAt(FIRST_INDEX);
    }

    /**
     * @return the creation date of this
     */
    public final String creationDate() {
        return matcher.group(DATE_GROUP);
    }
}
