package com.marcowillemart.common.domain;

/**
 * Factory represents a factory of objects of type T.
 *
 * @author Marco Willemart
 */
public interface Factory<T> {

    /**
     * @return a new object of type T.
     */
    T create();
}
