package com.marcowillemart.common.domain;

import static com.marcowillemart.common.test.Tests.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for the AbstractId class.
 *
 * @author Marco Willemart
 */
public class AbstractIdTest {

    private static final String ID_1 = createUUID();
    private static final String ID_2 = createUUID();

    private Identity target;
    private Identity same;
    private Identity other1;
    private Identity other2;

    @Before
    public void setUp() {
        target = new SampleId1(ID_1);
        same = new SampleId1(ID_1);
        other1 = new SampleId1(ID_2);
        other2 = new SampleId2(ID_1);
    }

    @Test
    public void testId() {
        // Exercise & Verify
        assertEquals(ID_1, target.id());
    }

    @Test
    public void testHashCode_same() {
        assertEquals(target.hashCode(), target.hashCode());
        assertEquals(same.hashCode(), target.hashCode());
    }

    @Test
    public void testHashCode_other() {
        assertNotEquals(other1.hashCode(), target.hashCode());
        assertNotEquals(other2.hashCode(), target.hashCode());
    }

    @Test
    public void testEquals_same() {
        assertTrue(target.equals(target));
        assertTrue(target.equals(same));
    }

    @Test
    public void testEquals_other() {
        assertFalse(target.equals(other1));
        assertFalse(target.equals(other2));
    }

    @Test
    public void testToString() {
        // Exercise
        String actual = target.toString();

        // Verify
        String expected = String.format("%s [id=%s]", "SampleId1", ID_1);
        assertEquals(expected, actual);
    }

    ////////////////////
    // INNER CLASSES
    ////////////////////

    private static class SampleId1 extends AbstractId {

        SampleId1(String id) {
            super(id);
        }
    }

    private static class SampleId2 extends AbstractId {

        SampleId2(String id) {
            super(id);
        }
    }
}
