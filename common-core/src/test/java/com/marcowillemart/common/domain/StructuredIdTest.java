package com.marcowillemart.common.domain;

import java.time.LocalDate;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for the StructuredId class.
 *
 * @author Marco Willemart
 */
public class StructuredIdTest {

    private static final String KEY = "SCL";
    private static final char CODE = 'I';

    private StructuredId target;

    @Before
    public void setUp() {
        target = new DummyStructuredId(
                IdentityGenerator.defaultInstance().newIdentity(KEY, CODE));
    }

    @Test
    public void testKey() {
        // Exercise & Verify
        assertEquals(KEY, target.key());
    }

    @Test
    public void testCode() {
        // Exercise & Verify
        assertEquals(CODE, target.code());
    }

    @Test
    public void testCreationDate() {
        // Exercise & Verify
        assertEquals(LocalDate.now().toString(), target.creationDate());
    }

    ////////////////////
    // INNER CLASS
    ////////////////////

    private static class DummyStructuredId extends StructuredId {

        DummyStructuredId(String id) {
            super(id);
        }
    } // end DummyStructuredId
}
