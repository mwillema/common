package com.marcowillemart.common.domain;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for the IdentityGenerator class.
 *
 * @author Marco Willemart
 */
public class IdentityGeneratorTest {

    private IdentityGenerator target;

    @Before
    public void setUp() {
        target = new IdentityGenerator();
    }

    @Test
    public void testNewIdentity_UUID() {
        // Exercise
        String id = target.newIdentity();

        // Verify
        assertEquals(36, id.length());
    }

    @Test
    public void testNewIdentity() {
        // Setup
        String key = "tur";
        char code = 'p';

        // Exercise
        String id = target.newIdentity(key, code);

        // Verify
        assertTrue(id.contains(key.toUpperCase()));
        assertTrue(id.contains(Character.toString(code).toUpperCase()));
        assertTrue(id.contains(LocalDate.now().toString()));
        assertEquals(29, id.length());
    }

    @Test
    public void testNewIdentity_many() throws InterruptedException {
        // Setup
        Set<String> ids = new HashSet<>();

        // Exercise
        for (int i = 0; i < 100_000; i++) {
            assertTrue(ids.add(target.newIdentity("tur", 'p')));
        }
    }
}
