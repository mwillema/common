package com.marcowillemart.common.domain;

import static com.marcowillemart.common.test.Tests.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for the Aggregate class.
 *
 * @author Marco Willemart
 */
public class AggregateTest {

    private static final String ID = createUUID();

    private Aggregate target;

    private Aggregate same;
    private Aggregate other1;
    private Aggregate other2;

    @Before
    public void setUp() {
        target = new DummyAggregate(ID);

        same = new DummyAggregate(ID);
        other1 = new DummyAggregate(createUUID());
        other2 = new DummyAggregate2(ID);
    }

    @Test
    public void testConstructor_Identity() {
        // Exercise
        target = new Aggregate(new AbstractId(ID) {}) {};

        // Verify
        assertEquals(ID, target.identity());
    }

    @Test
    public void testIdentity() {
        // Exercise & Verify
        assertEquals(ID, target.identity());
    }

    @Test
    public void testHashCode_same() {
        // Exercise & Verify
        assertEquals(target.hashCode(), target.hashCode());
        assertEquals(same.hashCode(), target.hashCode());
    }

    @Test
    public void testHashCode_different() {
        // Exercise & Verify
        assertNotEquals(other1.hashCode(), target.hashCode());
        assertNotEquals(other2.hashCode(), target.hashCode());
    }

    @Test
    public void testEquals_same() {
        // Exercise & Verify
        assertTrue(target.equals(target));
        assertTrue(target.equals(same));
    }

    @Test
    public void testEquals_different() {
        // Exercise & Verify
        assertFalse(target.equals(other1));
        assertFalse(target.equals(other2));
    }

    @Test
    public void testToString() {
        // Setup
        final String expected = "DummyAggregate [identity=" + ID + "]";

        // Exercise & Verify
        assertEquals(expected, target.toString());
    }

    ////////////////////
    // INNER CLASSES
    ////////////////////

    private static class DummyAggregate extends Aggregate {

        DummyAggregate(String identity) {
            super(identity);
        }
    } // end DummyAggregate

    private static class DummyAggregate2 extends Aggregate {

        DummyAggregate2(String identity) {
            super(identity);
        }
    } // end DummyAggregate2
}
