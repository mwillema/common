package com.marcowillemart.common.domain;

import static com.marcowillemart.common.test.Tests.*;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests for the CompositeId class.
 *
 * @author Marco Willemart
 */
public class CompositeIdTest {

    private CompositeId target;

    @Test
    public void testConstructor() {
        // Setup
        final String segment1 = createUUID();
        final String segment2 = createUUID();

        // Exercise
        target = new CompositeId(new DummyId(segment1), new DummyId(segment2));

        // Verify
        assertEquals(segment1 + ":" + segment2, target.id());
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private static class DummyId extends AbstractId {

        DummyId(String id) {
            super(id);
        }
    }
}
