package com.marcowillemart.common.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Tests is a utility class that provides useful methods for testing purposes.
 *
 * @author Marco Willemart
 */
public final class Tests {

    public static final boolean INCLUSIVE = true;
    public static final boolean EXCLUSIVE = !INCLUSIVE;

    private static final Random RANDOM = new Random();
    private static final int MAX_RANDOM_NUMBER = 100_001;

    /** this cannot be initialized */
    private Tests() {
        throw new AssertionError();
    }

    ////////////////////
    // CUSTOM ASSERTIONS
    ////////////////////

    ////////////////////
    // CREATION METHODS
    ////////////////////

    /**
     * @return a new UUID.
     */
    public static String createUUID() {
        return UUID.randomUUID().toString();
    }

    public static List<String> createUUIDs(int number) {
        List<String> uuids = new ArrayList<>(number);
        for (int i = 0; i < number; i++) {
            uuids.add(createUUID());
        }
        return uuids;
    }

    /**
     * @return a new pseudorandom number >= 0
     */
    public static int createRandomNumber() {
        return RANDOM.nextInt(MAX_RANDOM_NUMBER);
    }

    /**
     * @requires max > 0
     * @return a new pseudorandom number in range [0..max[
     */
    public static int createRandomNumber(int max) {
        return RANDOM.nextInt(max);
    }

    ////////////////////
    // UTILITY METHODS
    ////////////////////
}
