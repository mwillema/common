package com.marcowillemart.common.util;

import static com.marcowillemart.common.test.Tests.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests for the Lists class.
 *
 * @author Marco Willemart
 */
public class ListsTest {

    @Test
    public void testNewArrayList_empty() {
        // Exercise
        List<String> list = Lists.newArrayList();

        // Verify
        assertTrue(list.isEmpty());
    }

    @Test
    public void testNewArrayList_one() {
        // Setup
        final String element = "TheElement";
        List<String> expectedList = new LinkedList<>();
        expectedList.add(element);

        // Exercise
        List<String> actualList = Lists.newArrayList(element);

        // Exercise
        assertEquals("list", expectedList, actualList);
    }

    @Test
    public void testNewArrayList_many() {
        // Setup
        final String eltOne = "TheFirstElement";
        final String eltTwo = "TheSecondElement";
        List<String> expectedList = new LinkedList<>();
        expectedList.add(eltOne);
        expectedList.add(eltTwo);

        // Exercise
        List<String> actualList = Lists.newArrayList(eltOne, eltTwo);

        // Exercise
        assertEquals("list", expectedList, actualList);
    }

    @Test
    public void testNewLinkedList_empty() {
        // Exercise
        List<String> list = Lists.newLinkedList();

        // Verify
        assertTrue(list.isEmpty());
    }

    @Test
    public void testNewLinkedList_one() {
        // Setup
        final String element = "TheElement";
        List<String> expectedList = new LinkedList<>();
        expectedList.add(element);

        // Exercise
        List<String> actualList = Lists.newLinkedList(element);

        // Exercise
        assertEquals("list", expectedList, actualList);
    }

    @Test
    public void testNewLinkedList_many() {
        // Setup
        final String eltOne = "TheFirstElement";
        final String eltTwo = "TheSecondElement";
        List<String> expectedList = new LinkedList<>();
        expectedList.add(eltOne);
        expectedList.add(eltTwo);

        // Exercise
        List<String> actualList = Lists.newLinkedList(eltOne, eltTwo);

        // Exercise
        assertEquals("list", expectedList, actualList);
    }

    @Test
    public void testFirst() {
        // Setup
        final String eltOne = "TheFirstElement";
        final String eltTwo = "TheSecondElement";
        List<String> list = Lists.newArrayList(eltOne, eltTwo);

        // Exercise & Verify
        assertSame(eltOne, Lists.first(list));
    }

    @Test
    public void testLast_one() {
        // Setup
        final String element = "TheElement";
        List<String> list = Lists.newArrayList(element);

        // Exercise & Verify
        assertSame(element, Lists.last(list));
    }

    @Test
    public void testLast_many() {
        // Setup
        final String eltOne = "TheFirstElement";
        final String eltTwo = "TheSecondElement";
        List<String> list = Lists.newArrayList(eltOne, eltTwo);

        // Exercise & Verify
        assertSame(eltTwo, Lists.last(list));
    }

    @Test
    public void testToList() {
        // Setup
        List<String> expected = new ArrayList<>(createUUIDs(10));

        // Exercise
        List<String> actual = Lists.toList(expected.iterator());

        // Verify
        assertEquals(expected, actual);
    }

    @Test
    public void testMove_before() {
        // Setup
        final List<String> list = Lists.newArrayList("a", "b", "c", "d", "e");
        final List<String> expected =
                Lists.newArrayList("a", "d", "b", "c", "e");

        // Exercise
        String moved = Lists.move(list, 4, 2);

        // Verify
        assertEquals("d", moved);
        assertEquals(expected, list);
    }

    @Test
    public void testMove_after() {
        // Setup
        final List<String> list = Lists.newArrayList("a", "b", "c", "d", "e");
        final List<String> expected =
                Lists.newArrayList("a", "c", "d", "b",  "e");

        // Exercise
        String moved = Lists.move(list, 2, 4);

        // Verify
        assertEquals("b", moved);
        assertEquals(expected, list);
    }

    @Test
    public void testMove_samePlace() {
        // Setup
        final List<String> list = Lists.newArrayList("a", "b", "c", "d", "e");
        final List<String> expected =
                Lists.newArrayList("a", "b", "c", "d", "e");

        // Exercise
        String moved = Lists.move(list, 2, 2);

        // Verify
        assertEquals("b", moved);
        assertEquals(expected, list);
    }

    @Test
    public void testMove_firstToLast() {
        // Setup
        final List<String> list = Lists.newArrayList("a", "b", "c", "d", "e");
        final List<String> expected =
                Lists.newArrayList("b", "c", "d", "e", "a");

        // Exercise
        String moved = Lists.move(list, 1, 5);

        // Verify
        assertEquals("a", moved);
        assertEquals(expected, list);
    }

    @Test
    public void testMove_lastToFirst() {
        // Setup
        final List<String> list = Lists.newArrayList("a", "b", "c", "d", "e");
        final List<String> expected =
                Lists.newArrayList("e", "a", "b", "c", "d");

        // Exercise
        String moved = Lists.move(list, 5, 1);

        // Verify
        assertEquals("e", moved);
        assertEquals(expected, list);
    }
}
