package com.marcowillemart.common.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests for the Assert class.
 *
 * @author Marco Willemart
 */
public class AssertTest {

    @Test
    public void testIsDebug() {
        // Exercise & Verify
        assertTrue(Assert.isDebug());
    }

    @Test(expected = FailureException.class)
    public void testShouldNeverGetHere() {
        Assert.shouldNeverGetHere();
    }

    @Test(expected = FailureException.class)
    public void testFailOnUnexpectedException() {
        Assert.failOnUnexpectedException(new Exception("dummy exception"));
    }

    @Test
    public void testIsTrue() {
        try {
            Assert.isTrue(true);
        } catch (FailureException e) {
            fail("The assertion should be true");
        }
    }

    @Test(expected = FailureException.class)
    public void testIsTrue_false() {
        Assert.isTrue(false);
    }

    @Test
    public void testIsFalse() {
        try {
            Assert.isFalse(false);
        } catch (FailureException e) {
            fail("The assertion should be true");
        }
    }

    @Test(expected = FailureException.class)
    public void testIsFalse_true() {
        Assert.isFalse(true);
    }

    @Test
    public void testImplies() {
        Assert.implies(true, true);
        Assert.implies(false, true);
        Assert.implies(false, false);
    }

    @Test(expected = FailureException.class)
    public void testImplies_false() {
        Assert.implies(true, false);
    }

    @Test
    public void testEquiv() {
        Assert.equiv(true, true);
        Assert.equiv(false, false);
    }

    @Test(expected = FailureException.class)
    public void testEquiv_false_left() {
        Assert.equiv(false, true);
    }

    @Test(expected = FailureException.class)
    public void testEquiv_false_right() {
        Assert.equiv(true, false);
    }

    @Test
    public void testEquals_int_long() {
        try {
            Assert.equals(101, 101L);
        } catch (FailureException ex) {
            fail("The assertion should be true");
        }
    }

    @Test(expected = FailureException.class)
    public void testEquals_int_long_false() {
        Assert.equals(100, 101L);
    }

    @Test
    public void testEquals() {
        try {
            Assert.equals("dummy", "dummy");
        } catch (FailureException e) {
            fail("The assertion should be true");
        }
    }

    @Test
    public void testEquals_null() {
        Assert.equals(null, null);
    }

    @Test(expected = FailureException.class)
    public void testEquals_false() {
        Assert.equals("dummy1", "dummy2");
    }

    @Test(expected = FailureException.class)
    public void testEquals_false_leftNull() {
        Assert.equals(null, "dummy2");
    }

    @Test(expected = FailureException.class)
    public void testEquals_false_rightNull() {
        Assert.equals("dummy1", null);
    }

    @Test
    public void testNotEquals() {
        try {
            Assert.notEquals("dummy1", "dummy2");
        } catch (FailureException ex) {
            fail("The assertion should be true");
        }
    }

    @Test
    public void testNotEquals_leftNull() {
        Assert.notEquals(null, "dummy");
    }

    @Test
    public void testNotEquals_rightNull() {
        Assert.notEquals("dummy", null);
    }

    @Test(expected = FailureException.class)
    public void testNotEquals_false() {
        Assert.notEquals("dummy", "dummy");
    }

    @Test(expected = FailureException.class)
    public void testNotEquals_false_null() {
        Assert.notEquals(null, null);
    }

    @Test
    public void testNotEquals_long_int() {
        try {
            Assert.notEquals(101L, 100);
        } catch (FailureException ex) {
            fail("The assertion should be true");
        }
    }

    @Test(expected = FailureException.class)
    public void testNotEquals_long_int_false() {
        Assert.notEquals(100L, 100);
    }

    @Test
    public void testInRange() {
        Assert.inRange(10, 5, 15L);
    }

    @Test
    public void testInRange_min() {
        Assert.inRange(5, 5L, 15);
    }

    @Test
    public void testInRange_max() {
        Assert.inRange(15L, 5, 15);
    }

    @Test(expected = FailureException.class)
    public void testInRange_lessThanMin() {
        Assert.inRange(4, 5, 15);
    }

    @Test(expected = FailureException.class)
    public void testInRange_greaterThanMax() {
        Assert.inRange(16L, 5L, 15L);
    }

    @Test
    public void testIsNull() {
        try {
            Assert.isNull(null);
        } catch (FailureException e) {
            fail("The assertion should be true");
        }
    }

    @Test(expected = FailureException.class)
    public void testIsNull_notNull() {
        Assert.isNull(Boolean.TRUE);
    }

    @Test
    public void testNotNull() {
        try {
            Assert.notNull(Collections.EMPTY_LIST);
        } catch (FailureException e) {
            fail("The assertion should be true");
        }
    }

    @Test(expected = FailureException.class)
    public void testNotNull_null() {
        Assert.notNull(null);
    }

    @Test
    public void testNotEmpty() {
        try {
            Assert.notEmpty("toto");
        } catch (FailureException e) {
            fail("The assertion should be true");
        }
    }

    @Test(expected = FailureException.class)
    public void testNotEmpty_null() {
        // Setup
        String nullString = null;

        // Exercise
        Assert.notEmpty(nullString);
    }

    @Test(expected = FailureException.class)
    public void testNotEmpty_empty() {
        Assert.notEmpty("");
    }

    @Test
    public void testNotEmpty_collection() {
        List<String> notEmptyList = Arrays.asList("toto");
        try {
            Assert.notEmpty(notEmptyList);
        } catch (FailureException e) {
            fail("The assertion should be true");
        }
    }

    @Test(expected = FailureException.class)
    public void testNotEmpty_collection_null() {
        Assert.notEmpty((Collection<?>) null);
    }

    @Test(expected = FailureException.class)
    public void testNotEmpty_collection_empty() {
        Assert.notEmpty(Collections.emptyList());
    }

    @Test
    public void testNoNullElement() {
        List<String> list = Arrays.asList("1stElt", "2ndElt", " ");
        try {
            Assert.noNullElement(list);
        } catch (FailureException e) {
            fail("The assertion should be true");
        }
    }

    @Test
    public void testNoNullElement_emptyCollection() {
        try {
            Assert.noNullElement(Collections.emptyList());
        } catch (FailureException e) {
            fail("The assertion should be true");
        }
    }

    @Test(expected = FailureException.class)
    public void testNoNullElement_nullCollection() {
        // Setup
        Collection<?> nullCollection = null;

        // Exercise
        Assert.noNullElement(nullCollection);
    }

    @Test(expected = FailureException.class)
    public void testNoNullElement_nullValue() {
        List<String> list = Arrays.asList("1stElt", "2ndElt", null);
        Assert.noNullElement(list);
    }

    @Test
    public void testIsNumeric() {
        Assert.isNumeric("345");
    }

    @Test(expected = FailureException.class)
    public void testIsNumeric_nonNumeric() {
        Assert.isNumeric("3a5");
    }
}
