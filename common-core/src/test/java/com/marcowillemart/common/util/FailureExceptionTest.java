package com.marcowillemart.common.util;

import java.io.IOException;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests for the FailureException class.
 *
 * @author Marco Willemart
 */
public class FailureExceptionTest {

    @Test
    public void testFailureException() {
        // Exercise
        FailureException target = new FailureException();

        // Verify
        assertTrue(
                target.getMessage()
                        .contains("FailureExceptionTest.testFailureException"));
    }

    @Test
    public void testFailureException_cause() {
        // Setup
        final Throwable cause = new NullPointerException();

        // Exercise
        FailureException target = new FailureException(cause);

        // Verify
        assertTrue(
                target.getMessage()
                        .contains("testFailureException_cause"));
        assertSame(cause, target.getCause());
    }

    @Test
    public void testFailureException_message() {
        // Setup
        final String message = "dummy message";

        // Exercise
        FailureException target = new FailureException(message);

        // Verify
        assertEquals(message, target.getMessage());
    }

    @Test
    public void testFailureException_message_cause() {
        // Setup
        final String message = "dummy message";
        final Throwable cause = new IOException();

        // Exercise
        FailureException target = new FailureException(message, cause);

        // Verify
        assertEquals(message, target.getMessage());
        assertSame(cause, target.getCause());
    }
}
