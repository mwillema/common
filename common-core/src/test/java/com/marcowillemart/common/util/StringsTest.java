package com.marcowillemart.common.util;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests for the Strings class.
 *
 * @author Marco Willemart
 */
public class StringsTest {

    @Test
    public void testEmptyString() {
        assertTrue(Strings.EMPTY.isEmpty());
        assertEquals(0, Strings.EMPTY.length());
    }

    @Test
    public void testTrimQuotes() {
        // Setup
        final String text = "\"My text between \"quotes\"\"";

        // Exercise
        String actual = Strings.trimQuotes(text);

        // Verify
        String expected = "My text between \"quotes\"";
        assertEquals(expected, actual);
    }

    @Test
    public void testTrimQuotes_emptyText() {
        // Setup
        final String text = "\"\"";

        // Exercise
        String actual = Strings.trimQuotes(text);

        // Verify
        String expected = "";
        assertEquals(expected, actual);
    }

    @Test
    public void testTrimQuotes_noQuotes() {
        // Setup
        final String expected = "My text between \"quotes\"";

        // Exercise
        String actual = Strings.trimQuotes(expected);

        // Verify
        assertEquals(expected, actual);
    }

    @Test
    public void testTrimQuotes_single() {
        // Setup
        final String text = "'My text between 'quotes''";

        // Exercise
        String actual = Strings.trimQuotes(text);

        // Verify
        String expected = "My text between 'quotes'";
        assertEquals(expected, actual);
    }

    @Test
    public void testTrimQuotes_single_emptyText() {
        // Setup
        final String text = "''";

        // Exercise
        String actual = Strings.trimQuotes(text);

        // Verify
        String expected = "";
        assertEquals(expected, actual);
    }

    @Test
    public void testTrimQuotes_single_noQuotes() {
        // Setup
        final String expected = "My text between 'quotes'";

        // Exercise
        String actual = Strings.trimQuotes(expected);

        // Verify
        assertEquals(expected, actual);
    }

    @Test
    public void testIsNumeric() {
        assertFalse(Strings.isNumeric(null));
        assertFalse(Strings.isNumeric(""));
        assertFalse(Strings.isNumeric("  "));
        assertTrue(Strings.isNumeric("123"));
        assertTrue(Strings.isNumeric("\u0967\u0968\u0969"));
        assertFalse(Strings.isNumeric("12 3"));
        assertFalse(Strings.isNumeric("ab2c"));
        assertFalse(Strings.isNumeric("12-3"));
        assertFalse(Strings.isNumeric("12.3"));
        assertFalse(Strings.isNumeric("-123"));
        assertFalse(Strings.isNumeric("+123"));
    }

    @Test
    public void testHumanReadableByteCount() {
        assertEquals("1 B", Strings.humanReadableByteCount(1));
        assertEquals("10 B", Strings.humanReadableByteCount(10));
        assertEquals("100 B", Strings.humanReadableByteCount(100));
        assertEquals("1.0 kB", Strings.humanReadableByteCount(1_000));
        assertEquals("1.5 kB", Strings.humanReadableByteCount(1_500));
        assertEquals("1.0 MB", Strings.humanReadableByteCount(1_000_000));
        assertEquals("1.5 MB", Strings.humanReadableByteCount(1_500_000));
        assertEquals("1.0 GB", Strings.humanReadableByteCount(1_000_000_000));
        assertEquals("1.5 GB", Strings.humanReadableByteCount(1_500_000_000));
    }
}
