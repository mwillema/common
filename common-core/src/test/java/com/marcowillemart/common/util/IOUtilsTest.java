package com.marcowillemart.common.util;

import static com.marcowillemart.common.util.IOUtils.*;
import java.sql.Blob;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests for the IOUtils class.
 *
 * @author Marco Willemart
 */
public class IOUtilsTest {

    @Test
    public void testSerialize() {
        // Setup
        String expected = "toto";

        // Exercise
        byte[] buffer = serialize(expected);

        // Verify
        Object actual = deserialize(buffer);
        assertEquals(expected, actual);
    }

    @Test
    public void testToBlob() throws Exception {
        // Setup
        Object obj = "dummyObject";
        byte[] buffer = serialize(obj);

        // Exercise
        Blob blob = toBlob(buffer);

        // Verify
        assertEquals(obj, deserialize(blob.getBinaryStream()));
    }

    @Test
    public void testToBytes() {
        // Setup
        Object obj = "dummyObject";
        byte[] expected = serialize(obj);
        Blob blob = toBlob(expected);

        // Exercise
        byte[] actual = toByteArray(blob);

        // Verify
        assertArrayEquals(expected, actual);
    }
}
