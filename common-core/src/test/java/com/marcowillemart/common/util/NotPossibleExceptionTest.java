package com.marcowillemart.common.util;

import java.util.Arrays;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests for the NotPossibleException class.
 *
 * @author Marco Willemart
 */
public class NotPossibleExceptionTest {

    @Test
    public void testNotPossibleException() {
        // Setup
        final String message = "dummy message";

        // Exercise
        NotPossibleException target = new NotPossibleException(message);

        // Verify
        assertEquals(message, target.getMessage());
        assertNull(target.getCode());
        assertTrue(target.getArguments().isEmpty());
    }

    @Test
    public void testNotPossibleException_code() {
        // Setup
        final String message = "dummy message";
        final String code = "dummy.code";

        // Exercise
        NotPossibleException target = new NotPossibleException(message, code);

        // Verify
        assertEquals(message, target.getMessage());
        assertEquals(code, target.getCode());
        assertTrue(target.getArguments().isEmpty());
    }

    @Test
    public void testNotPossibleException_arguments() {
        // Setup
        final String message = "dummy message";
        final String code = "dummy.code";
        final int argument1 = 3;
        final String argument2 = "dummy arg";

        // Exercise
        NotPossibleException target = new NotPossibleException(
                message,
                code,
                argument1,
                argument2);

        // Verify
        assertEquals(message, target.getMessage());
        assertEquals(code, target.getCode());
        assertEquals(
                Arrays.asList(argument1, argument2),
                target.getArguments());
    }
}
