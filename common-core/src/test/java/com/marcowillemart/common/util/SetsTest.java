package com.marcowillemart.common.util;

import static com.marcowillemart.common.test.Tests.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests for the Sets class.
 *
 * @author Marco Willemart
 */
public class SetsTest {

    @Test
    public void testNewHashSet_empty() {
        // Exercise
        Set<String> set = Sets.newHashSet();

        // Verify
        assertTrue(set.isEmpty());
    }

    @Test
    public void testNewHashSet_one() {
        // Setup
        final String element = "TheElement";
        Set<String> expectedSet = new HashSet<>();
        expectedSet.add(element);

        // Exercise
        Set<String> actualSet = Sets.newHashSet(element);

        // Exercise
        assertEquals("set", expectedSet, actualSet);
    }

    @Test
    public void testNewHashSet_multiple() {
        // Setup
        final String eltOne = "TheFirstElement";
        final String eltTwo = "TheSecondElement";
        Set<String> expectedSet = new HashSet<>();
        expectedSet.add(eltOne);
        expectedSet.add(eltTwo);

        // Exercise
        Set<String> actualSet = Sets.newHashSet(eltOne, eltTwo);

        // Exercise
        assertEquals("set", expectedSet, actualSet);
    }

    @Test
    public void testLinkedNewHashSet_empty() {
        // Exercise
        Set<String> set = Sets.newLinkedHashSet();

        // Verify
        assertTrue(set.isEmpty());
    }

    @Test
    public void testNewLinkedHashSet_one() {
        // Setup
        final String element = "TheElement";
        Set<String> expectedSet = new LinkedHashSet<>();
        expectedSet.add(element);

        // Exercise
        Set<String> actualSet = Sets.newLinkedHashSet(element);

        // Verify
        assertEquals("set", expectedSet, actualSet);
    }

    @Test
    public void testNewLinkedHashSet_multiple() {
        // Setup
        final String eltOne = "TheFirstElement";
        final String eltTwo = "TheSecondElement";
        Set<String> expectedSet = new LinkedHashSet<>();
        expectedSet.add(eltOne);
        expectedSet.add(eltTwo);

        // Exercise
        Set<String> actualSet = Sets.newLinkedHashSet(eltOne, eltTwo);

        // Verify
        assertEquals("set", expectedSet, actualSet);
        assertEquals(expectedSet.toString(), actualSet.toString());
    }

    @Test
    public void testSubSet() {
        // Setup
        Set<String> set = new LinkedHashSet<>();
        set.add("toto");
        set.add("tata");
        set.add("tito");

        Filterer<String> filter = new Filterer<String>() {

            @Override
            public boolean check(String t) {
                return t.contains("o");
            }
        };

        Set<String> expected = Sets.newHashSet("toto", "tito");

        // Exercise
        Set<String> actual = Sets.subSet(set, filter);

        // Verify
        assertEquals("subset", expected, actual);
    }

    @Test
    public void testToSet() {
        // Setup
        List<String> expected = new ArrayList<>(createUUIDs(10));

        // Exercise
        Set<String> actual = Sets.toSet(expected.iterator());

        // Verify
        assertEquals(expected, new ArrayList<>(actual));
    }
}
