package com.marcowillemart.common.util;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for the DataTable class.
 *
 * @author Marco Willemart
 */
public class DataTableTest {

    private static final String NAME = "MyTableName";

    private DataTable target;

    private List<String> columnNames;

    @Before
    public void setUp() {
        columnNames = Arrays.asList("C1", "C2", "C3");

        target = new DataTable(NAME, columnNames);
    }

    @Test
    public void testName() {
        assertEquals(NAME, target.name());
    }

    @Test
    public void testColumnSize() {
        assertEquals(columnNames.size(), target.columnSize());
    }

    @Test
    public void testColumnNames() {
        assertEquals(columnNames, target.columnNames());
    }

    @Test
    public void testAddRow() {
        // Setup
        List<String> row = Arrays.asList("hello", "bonjour", "hola");

        // Exercise
        target.addRow(row);

        // Verify
        Iterator<List<?>> iterator = target.iterator();

        assertTrue(iterator.hasNext());
        assertEquals(columnNames, iterator.next());
        assertTrue(iterator.hasNext());
        assertEquals(row, iterator.next());
        assertFalse(iterator.hasNext());
    }

    @Test
    public void testAddRow_null() {
        // Setup
        List<?> row = Arrays.asList(3, true, null);

        // Exercise
        target.addRow(row);

        // Verify
        Iterator<List<?>> iterator = target.iterator();

        assertTrue(iterator.hasNext());
        assertEquals(columnNames, iterator.next());
        assertTrue(iterator.hasNext());
        assertEquals(row, iterator.next());
        assertFalse(iterator.hasNext());
    }

    @Test
    public void testIterator_zeroRow() {
        // Exercise
        Iterator<List<?>> generator = target.iterator();

        // Verify
        assertTrue(generator.hasNext());
        assertEquals(columnNames, generator.next());
        assertFalse(generator.hasNext());
    }

    @Test
    public void testIterator_multipleRows() {
        // Setup
        List<String> row1 = Arrays.asList("hello", "bonjour", "hola");
        List<String> row2 = Arrays.asList("goodbye", "au revoir", "adios");
        target.addRow(row1);
        target.addRow(row2);

        // Exercise
        Iterator<List<?>> iterator = target.iterator();

        // Verify
        assertTrue(iterator.hasNext());
        assertEquals(columnNames, iterator.next());
        assertTrue(iterator.hasNext());
        assertEquals(row1, iterator.next());
        assertTrue(iterator.hasNext());
        assertEquals(row2, iterator.next());
        assertFalse(iterator.hasNext());
    }
}
