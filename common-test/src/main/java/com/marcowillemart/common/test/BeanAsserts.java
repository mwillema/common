package com.marcowillemart.common.test;

import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;

/**
 * BeanAsserts is a utility class providing methods for testing beans.
 *
 * @author Marco Willemart
 */
final class BeanAsserts {

    /** this cannot be instantiated. */
    private BeanAsserts() {
        throw new AssertionError();
    }

    /**
     * @effects Tests the bean of type 'requiredType' exists and is not null
     *          in the given application context.
     */
    static <T> void assertBeanExists(Class<T> requiredType,
            ApplicationContext applicationContext) {
        assertNotNull(applicationContext.getBean(requiredType));
    }

    /**
     * @effects Tests that bean of type 'requiredType' has a singleton scope
     *          in the given application context.
     */
    static <T> void testBeanScope_singleton(Class<T> requiredType,
            ApplicationContext applicationContext) {
        T beanOne = applicationContext.getBean(requiredType);
        T beanTwo = applicationContext.getBean(requiredType);

        assertSame(requiredType.getSimpleName(), beanOne, beanTwo);
    }

    /**
     * @effects Tests that bean of type 'requiredType' has a prototype scope
     *          in the given application context.
     */
    static <T> void testBeanScope_prototype(Class<T> requiredType,
            ApplicationContext applicationContext) {
        T beanOne = applicationContext.getBean(requiredType);
        T beanTwo = applicationContext.getBean(requiredType);

        assertNotSame(requiredType.getSimpleName(), beanOne, beanTwo);
    }
}
