package com.marcowillemart.common.test;

import com.google.protobuf.Message;

/**
 * EventMatcher represents an abstract matcher for published events.
 *
 * @author Marco Willemart
 */
public abstract class EventMatcher {

    /**
     * @requires event != null
     * @return true iff the given event matches this
     */
    abstract boolean matches(Message event);

    /**
     * @requires event != null && !matches(event)
     * @return the mistmatch description for the given event
     */
    abstract String mistmatchDescription(Message event);
}
