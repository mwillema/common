package com.marcowillemart.common.test;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * MockInjector is a utility class used for testing that replaces beans by
 * mock objects in a Spring context.
 *
 * @author Marco Willemart
 */
public class MockInjector implements BeanPostProcessor {

    private static final Map<String, Object> MOCKS = new HashMap<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName)
            throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName)
            throws BeansException {
        if (MOCKS.containsKey(beanName)) {
            return MOCKS.get(beanName);
        }
        return bean;
    }

    /**
     * @requires beanName != null && mock != null
     * @modifies this
     * @effects Adds the mock 'mock' for the bean named 'beanName' to this.
     */
    public static void addMock(String beanName, Object mock) {
        MOCKS.put(beanName, mock);
    }

    /**
     * @modifies this
     * @effects Clears the mocks in this.
     */
    public static void clear() {
        MOCKS.clear();
    }
}
