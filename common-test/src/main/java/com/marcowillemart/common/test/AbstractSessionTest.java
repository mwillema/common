package com.marcowillemart.common.test;

import com.marcowillemart.common.test.AbstractContextTest;
import static org.junit.Assert.*;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.web.context.request.AbstractRequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * AbstractSessionTest is an abstract class that mocks HTTP sessions and
 * requests for testing request and session scoped beans.
 *
 * @author Marco Willemart
 */
public abstract class AbstractSessionTest extends AbstractContextTest {

    private MockHttpSession session;
    private MockHttpServletRequest request;

    protected final void startSession() {
        session = new MockHttpSession();
    }

    protected final void endSession() {
        session.clearAttributes();
        session = null;
    }

    protected final void startRequest() {
        request = new MockHttpServletRequest();
        request.setSession(session);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(
                request));
    }

    protected final void endRequest() {
        ((AbstractRequestAttributes)
                RequestContextHolder.getRequestAttributes()).requestCompleted();
        RequestContextHolder.resetRequestAttributes();
        request = null;
    }

    /**
     * @effects Tests that two beans of type 'requiredType' retrieved during
     *          the same session are same.
     */
    protected <T> void testBeanScope_sameSession(Class<T> requiredType) {
        startSession();

        startRequest();
        T beanOne = applicationContext.getBean(requiredType);
        endRequest();

        startRequest();
        T beanTwo = applicationContext.getBean(requiredType);
        endRequest();

        endSession();

        assertSame(requiredType.getSimpleName(), beanOne, beanTwo);
    }

    /**
     * @effects Tests that two beans of type 'requiredType' retrieved during
     *          different sessions are not same.
     */
    protected <T> void testBeanScope_differentSession(Class<T> requiredType) {
        startSession();
        startRequest();
        T beanOne = applicationContext.getBean(requiredType);
        endRequest();
        endSession();

        startSession();
        startRequest();
        T beanTwo = applicationContext.getBean(requiredType);
        endRequest();
        endSession();

        assertNotSame(requiredType.getSimpleName(), beanOne, beanTwo);
    }
}
