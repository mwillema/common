package com.marcowillemart.common.test;

import com.google.protobuf.Descriptors.Descriptor;
import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.Message;
import com.google.protobuf.ProtocolMessageEnum;
import java.util.Arrays;
import static org.junit.Assert.*;

/**
 * WithField is an event matcher that matches event fields.
 *
 * @author Marco Willemart
 */
public final class WithField extends EventMatcher {

    private final int fieldNumber;
    private final Object expectedValue;

    /**
     * @requires fieldNumber is a valid field number of its event type &&
     *           expectedValue != null
     * @effects Makes this be a new WithField matcher with the given field
     *          number and expected value
     */
    private WithField(int fieldNumber, Object expectedValue) {
        this.fieldNumber = fieldNumber;
        this.expectedValue = expectedValue;
    }

    @Override
    boolean matches(Message event) {
        Object actualValue = actualValueOf(event);

        return expectedValue.equals(actualValue);
    }

    @Override
    String mistmatchDescription(Message event) {
        return String.format("%s.%s expected: %s but was: %s",
                event.getDescriptorForType().getName(),
                targetedFieldDescriptorOf(event).getName(),
                expectedValue,
                actualValueOf(event));
    }

    public static EventMatcher withField(
            int fieldNumber,
            Object expectedValue) {

        return new WithField(fieldNumber, refineValues(expectedValue)[0]);
    }

    public static EventMatcher withRepeatedField(
            int fieldNumber,
            Object... expectedValues) {

        return new WithField(
                fieldNumber,
                Arrays.asList(refineValues(expectedValues)));
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    /**
     * @requires event != null
     * @return the actual value of the matched field of event
     */
    private Object actualValueOf(Message event) {
        FieldDescriptor fieldDescriptor = targetedFieldDescriptorOf(event);
        return event.getField(fieldDescriptor);
    }

    /**
     * @requires event != null
     * @return the descriptor of the matched field of event
     */
    private FieldDescriptor targetedFieldDescriptorOf(Message event) {
        Descriptor descriptor = event.getDescriptorForType();

        FieldDescriptor fieldDescriptor =
                descriptor.findFieldByNumber(fieldNumber);

        assertNotNull(fieldDescriptor);

        return fieldDescriptor;
    }

    /**
     * @requires for all v in values, v != null
     * @return the given values refined for comparison
     */
    private static Object[] refineValues(Object... values) {
        Object[] refinedValues = new Object[values.length];

        for (int i = 0; i < values.length; i++) {
            Object value = values[i];

            if (value instanceof ProtocolMessageEnum) {
                refinedValues[i] =
                        ((ProtocolMessageEnum) value).getValueDescriptor();
            } else {
                refinedValues[i] = value;
            }
        }

        return refinedValues;
    }
}
