package com.marcowillemart.common.test;

import com.google.protobuf.Descriptors.Descriptor;
import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.Message;
import com.marcowillemart.common.concurrent.GuardedBy;
import com.marcowillemart.common.concurrent.ThreadSafe;
import com.marcowillemart.common.util.FailureException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;

/**
 * DomainEventCatcher is a thread-safe catcher of published events. In addition,
 * it provides assertion methods to verify what has been published.
 *
 * @author Marco Willemart
 */
@ThreadSafe
public class DomainEventCatcher {

    private static final Logger LOG =
            LoggerFactory.getLogger(DomainEventCatcher.class);

    @GuardedBy("events") private final List<Message> events;

    /**
     * @effects Makes this be a new domain event catcher.
     */
    public DomainEventCatcher() {
        this.events = Collections.synchronizedList(new LinkedList<Message>());
    }

    /**
     * @requires event != null
     * @modifies this
     * @effects Adds the given event to the caught events of this
     */
    @EventListener
    public void when(Message event) {
        events.add(event);

        LOG.trace("Domain event: {}", event.getClass().getSimpleName());
    }

    /**
     * @requires eventType != null
     * @modifies this
     * @effects Waits until an event of the given type has been published.
     */
    public void waitUntil(Class<? extends Message> eventType) {
        while (!isPublished(eventType)) {
            sleep();
        }
    }

    /**
     * @requires expected != null
     * @effects Asserts the expected message has been published.
     */
    public void assertPublished(Message expected) {
        if (events.contains(expected)) {
            return;
        }

        assertPublished(expected.getClass());

        Message actual = lastPublished(expected.getClass());

        Descriptor descriptor = expected.getDescriptorForType();

        for (FieldDescriptor field : descriptor.getFields()) {
            assertEquals(
                    descriptor.getName() + "." + field.getName(),
                    expected.getField(field),
                    actual.getField(field));
        }
    }

    /**
     * @requires eventType != null
     * @effects Asserts an event of the given type has been published.
     */
    public void assertPublished(Class<? extends Message> eventType) {
        assertTrue(
                String.format(
                        "A %s domain event should be published",
                        eventType.getSimpleName()),
                isPublished(eventType));
    }

    /**
     * @requires eventType != null && for all m in matchers, m != null
     * @effects Asserts an event of the given type matching the given matchers
     *          has been published.
     */
    public void assertPublished(
            Class<? extends Message> eventType,
            EventMatcher... matchers) {

        assertPublished(eventType);

        List<Message> candidates = publishedEventsOf(eventType);

        int maxMatches = 0;
        Message bestCandidate = null;

        for (Message candidate : candidates) {
            int successfulMatches = 0;

            for (EventMatcher matcher : matchers) {
                if (matcher.matches(candidate)) {
                    successfulMatches++;
                } else {
                    break;
                }
            }

            if (successfulMatches == matchers.length) {
                return;
            }

            if (successfulMatches >= maxMatches) {
                maxMatches = successfulMatches;
                bestCandidate = candidate;
            }
        }

        EventMatcher failedMatcher = matchers[maxMatches];
        fail(failedMatcher.mistmatchDescription(bestCandidate));
    }

    /**
     * @modifies this
     * @effects Clears the published events
     */
    public void clear() {
        events.clear();
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    /**
     * @requires eventType != null
     * @return true iff an event of the given type has been published
     */
    private boolean isPublished(Class<? extends Message> eventType) {
        boolean found = false;

        synchronized (events) {
            for (Message event : events) {
                if (event.getClass().equals(eventType)) {
                    found = true;
                    break;
                }
            }
        }

        return found;
    }

    /**
     * @requires eventType != null && isPublished(eventType)
     * @return the last published event of the given type
     */
    @SuppressWarnings("unchecked")
    private <T extends Message> T lastPublished(Class<T> eventType) {

        synchronized (events) {
            ListIterator<Message> gen = events.listIterator(events.size());

            while (gen.hasPrevious()) {
                Message message = gen.previous();

                if (message.getClass().equals(eventType)) {
                    return (T) message;
                }
            }
        }

        throw new FailureException();
    }

    /**
     * @requires eventType != null
     * @return the published events of the given type
     */
    private List<Message> publishedEventsOf(
            Class<? extends Message> eventType) {

        List<Message> list = new LinkedList<>();

        synchronized (events) {
            for (Message event : events) {
                if (event.getClass().equals(eventType)) {
                    list.add(event);
                }
            }
        }

        return list;
    }

    /**
     * @effects Sleeps for a short period of time
     */
    private static void sleep() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            fail(ex.getMessage());
        }
    }
}
