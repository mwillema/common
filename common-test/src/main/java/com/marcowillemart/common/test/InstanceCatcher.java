package com.marcowillemart.common.test;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

/**
 * InstanceCatcher matches object instances.
 *
 * @author Marco Willemart
 */
public class InstanceCatcher<T> extends BaseMatcher<T> {

    private T instance;

    /**
     * @return the instance of this.
     */
    public T instance() {
        return instance;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean matches(Object o) {
        try {
            instance = (T) o;
            return true;
        } catch (ClassCastException ex) {
            return false;
        }
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("any old Object");
    }
}
