package com.marcowillemart.common.test;

import org.jmock.integration.junit4.JUnitRuleMockery;
import org.jmock.lib.concurrent.Synchroniser;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Rule;

/**
 * AbstractMockTest is an abstract class that provides the basics to
 * performs tests using mock objects.
 *
 * @author Marco Willemart
 */
public abstract class AbstractMockTest {

    /** The JUnit rule that manages JMock expectations and allowances. */
    @Rule
    public final JUnitRuleMockery context = new JUnitRuleMockery() {{
        setImposteriser(ClassImposteriser.INSTANCE);
        setThreadingPolicy(new Synchroniser());
    }};
}
