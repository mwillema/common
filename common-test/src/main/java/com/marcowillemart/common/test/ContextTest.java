package com.marcowillemart.common.test;

/**
 * ContextText defines utility methods to test Spring application contexts.
 *
 * @author Marco Willemart
 */
public interface ContextTest {

    /**
     * @effects Tests the bean of type 'requiredType' exists and is not null.
     */
    <T> void assertBeanExists(Class<T> requiredType);

    /**
     * @effects Tests that bean of type 'requiredType' has a singleton scope.
     */
    <T> void testBeanScope_singleton(Class<T> requiredType);

    /**
     * @effects Tests that bean of type 'requiredType' has a prototype scope.
     */
    <T> void testBeanScope_prototype(Class<T> requiredType);
}
