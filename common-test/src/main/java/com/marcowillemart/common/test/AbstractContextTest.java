package com.marcowillemart.common.test;

import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

/**
 * AbstractContextTest is an abstract class that provides utility methods to
 * test Spring application contexts.
 *
 * @author Marco Willemart
 */
@TestPropertySource("/test.properties")
public abstract class AbstractContextTest
        extends AbstractJUnit4SpringContextTests implements ContextTest {

    @Override
    public <T> void assertBeanExists(Class<T> requiredType) {
        BeanAsserts.assertBeanExists(requiredType, applicationContext);
    }

    @Override
    public <T> void testBeanScope_singleton(Class<T> requiredType) {
        BeanAsserts.testBeanScope_singleton(requiredType, applicationContext);
    }

    @Override
    public <T> void testBeanScope_prototype(Class<T> requiredType) {
        BeanAsserts.testBeanScope_prototype(requiredType, applicationContext);
    }
}
