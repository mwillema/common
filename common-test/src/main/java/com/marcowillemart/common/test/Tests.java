package com.marcowillemart.common.test;

import com.google.protobuf.Message;
import com.marcowillemart.common.util.Lists;
import com.marcowillemart.common.util.Sets;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import static org.junit.Assert.*;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Tests is a utility class that provides useful methods for testing purposes.
 *
 * @author Marco Willemart
 */
public final class Tests {

    private static final String USER = "user";
    private static final String PASSWORD = "password";
    private static final List<GrantedAuthority> AUTHORITIES =
            AuthorityUtils.createAuthorityList("ROLE_USER");

    private static final String ANONYMOUS_USER = "anonymousUser";
    private static final List<GrantedAuthority> ANONYMOUS_AUTHORITIES =
            AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS");

    private static final Random RANDOM = new Random();
    private static final int MAX_RANDOM_NUMBER = 100_001;

    /** this cannot be initialized */
    private Tests() {
        throw new AssertionError();
    }

    ////////////////////
    // CUSTOM ASSERTIONS
    ////////////////////

    /**
     * @requires expected != null && actual != null
     * @effects Asserts the expected and actual BigDecimals are equals.
     */
    public static void assertBigDecimalEquals(BigDecimal expected,
            BigDecimal actual) {
        if (expected == null || actual == null) {
            assertEquals(expected, actual);
        } else {
            assertEquals(0, expected.compareTo(actual));
        }
    }

    /**
     * @requires expected != null && actual != null
     * @effects Asserts the expected collection equals the actual collection.
     */
    public static <T> void assertCollectionEquals(
            Collection<? extends T> expected, Collection<? extends T> actual) {
        assertEquals("collection.size", expected.size(), actual.size());
        assertTrue("The collections should be equals",
                expected.containsAll(actual));
    }

    /**
     * @requires map, key and value not null
     * @effects Asserts the entry <key,value> exists in map.
     */
    public static <K,V> void assertContains(Map<K,V> map, K key, V value) {
        assertTrue(map.containsKey(key));
        assertEquals(value, map.get(key));
    }

    /**
     * @requires expected != null && actual != null
     * @modifies actual
     * @effects Asserts the expected and actual generators produce the same
     *          elements in any order.
     */
    public static <T> void assertGeneratorEquals(Set<T> expected,
            Iterator<T> actual) {
        Set<T> set = Sets.toSet(actual);
        assertEquals(expected, set);
    }

    /**
     * @requires expected != null && actual != null
     * @modifies actual
     * @effects Asserts the expected and actual generators produce the same
     *          elements in the same order.
     */
    public static <T> void assertGeneratorEquals(List<T> expected,
            Iterator<T> actual) {
        List<T> list = Lists.toList(actual);
        assertEquals(expected, list);
    }

    ////////////////////
    // CREATION METHODS
    ////////////////////

    public static Dto.Sample createAnonymousDtoSample() {
        return Dto.Sample.newBuilder()
                .setId(createRandomNumber())
                .setName(createUUID())
                .build();
    }

    public static Message createAnonymousMessage() {
        return createAnonymousDtoSample();
    }

    public static List<Message> createAnonymousMessages(int nbOfMessages) {
        List<Message> messages = new ArrayList<>(nbOfMessages);

        for (int i = 0; i < nbOfMessages; i++) {
            messages.add(createAnonymousMessage());
        }

        return messages;
    }

    /**
     * @return a new anonymous name n such that n matches [a-zA-Z][A-Za-z_0-9]*
     */
    public static String createAnonymousName() {
        String[] uuid = createUUID().split("-");
        return "a" + uuid[uuid.length- 1];
    }

    /**
     * @return a new UUID.
     */
    public static String createUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * @return a new pseudorandom number >= 0
     */
    public static int createRandomNumber() {
        return RANDOM.nextInt(MAX_RANDOM_NUMBER);
    }

    /**
     * @requires max > 0
     * @return a new pseudorandom number in range [0..max[
     */
    public static int createRandomNumber(int max) {
        return RANDOM.nextInt(max);
    }

    /**
     * @return a new pseudorandom number > 0
     */
    public static int createPositiveRandomNumber() {
        return createRandomNumber() + 1;
    }

    /**
     * @return a new authentication with username "user", password "password"
     *         and role "ROLE_USER".
     */
    public static Authentication createAuthentication() {
        return createAuthentication(USER, PASSWORD);
    }

    /**
     * @requires user != null && password != null
     * @return a new authentication with username 'username', password
     *         'password' and role "ROLE_USER".
     */
    public static Authentication createAuthentication(
            String username,
            String password) {

        return createAuthentication(username, password, AUTHORITIES);
    }

    /**
     * @return a new authentication with username "anonymousUser", password ""
     *         and role "ROLE_ANONYMOUS".
     */
    public static Authentication createAnonymousAuthentication() {
        return new AnonymousAuthenticationToken(
                "key",
                ANONYMOUS_USER,
                ANONYMOUS_AUTHORITIES);
    }

    /**
     * @return a new authentication with username 'principal',
     *         password 'credentials' and roles 'authorities'.
     */
    public static Authentication createAuthentication(
            Object principal,
            Object credentials,
            Collection<? extends GrantedAuthority> authorities) {

        return new UsernamePasswordAuthenticationToken(
                principal,
                credentials,
                authorities);
    }

    public static UserDetails createUserDetails(String username) {
        return new User(
                username,
                createUUID(),
                AUTHORITIES);
    }

    ////////////////////
    // UTILITY METHODS
    ////////////////////

    public static void configureAuthentication() {
        configureAuthentication(createAuthentication());
    }

    public static void configureAnonymousAuthentication() {
        configureAuthentication(createAnonymousAuthentication());
    }

    public static void configureAuthentication(Authentication token) {
        SecurityContextHolder.getContext().setAuthentication(token);
    }

    public static void configureAuthentication(Object principal,
            Object credentials) {
        configureAuthentication(principal, credentials, AUTHORITIES);
    }

    public static void configureAuthentication(
            Object principal,
            Object credentials,
            Collection<? extends GrantedAuthority> authorities) {

        configureAuthentication(
                createAuthentication(principal, credentials, authorities));
    }

    public static void configureAuthentication(Object principal,
            Object credentials, Object details) {
        AbstractAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(
                        principal,
                        credentials,
                        AUTHORITIES);

        token.setDetails(details);

        configureAuthentication(token);
    }

    public static void clearAuthentication() {
        SecurityContextHolder.clearContext();
    }

    /**
     * @requires obj and name not null && obj instanceof T &&
     *           name names a field of obj
     * @return the field name named of the object obj (as type).
     */
    @SuppressWarnings("unchecked")
    public static <T> T getField(Object obj, String name) {
        try {
            Field field = obj.getClass().getDeclaredField(name);
            field.setAccessible(true);
            return (T) field.get(obj);
        } catch (IllegalAccessException | IllegalArgumentException
                | NoSuchFieldException | SecurityException ex) {
            throw new RuntimeException(ex);
        }
    }
}
