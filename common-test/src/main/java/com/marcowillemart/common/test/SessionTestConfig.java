package com.marcowillemart.common.test;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Session test configuration.
 *
 * @author Marco Willemart
 */
@Configuration
@ImportResource("classpath:abstractSessionTest.xml")
public class SessionTestConfig {
}
