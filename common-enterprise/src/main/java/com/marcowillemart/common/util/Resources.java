package com.marcowillemart.common.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;

/**
 * Resources is a utility class that provides useful methods for manipulating
 * {@link Resource} instances.
 *
 * @author Marco Willemart
 */
public final class Resources {

    /** this cannot be instantiated */
    private Resources() {
        throw new AssertionError();
    }

    /**
     * @requires str != null
     * @return a new resource for the given string.
     */
    public static Resource from(String str) {
        Assert.notNull(str);

        return new ByteArrayResource(str.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * @requires resource != null && resource is readable
     * @return resource (as File)
     */
    public static File toFile(Resource resource) {
        Assert.notNull(resource);
        Assert.isTrue(resource.isReadable());

        if (resource.isFile()) {
            try {
                return resource.getFile();
            } catch (IOException ex) {
                throw new FailureException(ex);
            }
        }

        // If you try to use resource.getFile() you will receive an error,
        // because Spring tries to access a file system path, but it can not
        // access a path in your JAR.

        try {
            File file = File.createTempFile("marcowillemart-", ".tmp");
            file.deleteOnExit();

            IOUtils.copy(resource.getInputStream(), file);

            return file;
        } catch (IOException ex) {
            throw new FailureException(ex);
        }
    }
}
