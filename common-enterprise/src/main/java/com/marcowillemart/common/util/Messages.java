package com.marcowillemart.common.util;

import com.google.protobuf.Any;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;

/**
 * Messages is a utility class that provides useful methods for manipulating
 * {@link com.google.protobuf.Message} instances.
 *
 * @author Marco Willemart
 */
public final class Messages {

    private Messages() {
        throw new FailureException();
    }

    /**
     * @requires typeUrl != null && typeUrl contains at least one '/'
     * @return the fully qualified type name after the last '/' in typeUrl
     */
    public static String typeNameOf(String typeUrl) {
        Assert.notNull(typeUrl);

        String[] parts = typeUrl.split("/");

        Assert.isTrue(parts.length > 1);

        return parts[parts.length - 1];
    }

    /**
     * @requires packageName != null && typeName != null &&
     *           packageName.typeName must name an existing class
     * @return the class named packageName.typeName
     */
    public static <T extends Message> Class<T> classForName(
            String packageName,
            String typeName) {

        return classForName(String.format("%s.%s", packageName, typeName));
    }

    /**
     * @requires any != null && type != null && type is the Message type of any
     * @return the unpacked Message of the given type from any
     */
    public static <T extends Message> T unpack(Any any, Class<T> type) {
        try {
            return any.unpack(type);
        } catch (InvalidProtocolBufferException ex) {
            throw new FailureException(
                    String.format(
                            "Cannot unpack message of type %s",
                            type.getName()),
                    ex);
        }
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    @SuppressWarnings("unchecked")
    private static <T extends Message> Class<T> classForName(String className) {
        try {
            return (Class<T>) Class.forName(className);
        } catch (ClassNotFoundException ex) {
            throw new FailureException(ex);
        }
    }
}
