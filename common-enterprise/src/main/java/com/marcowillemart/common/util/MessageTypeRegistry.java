package com.marcowillemart.common.util;

import com.google.protobuf.Descriptors.Descriptor;
import com.google.protobuf.util.JsonFormat;
import com.marcowillemart.common.concurrent.GuardedBy;
import com.marcowillemart.common.concurrent.ThreadSafe;
import java.util.HashSet;
import java.util.Set;

/**
 * MessageTypeRegistry represents a mutable registry of Message types.
 *
 * @author Marco Willemart
 */
@ThreadSafe
public class MessageTypeRegistry {

    @GuardedBy("this") private final Set<Descriptor> descriptors;
    @GuardedBy("this") private volatile JsonFormat.TypeRegistry typeRegistry;

    /**
     * @effects Makes this be a new empty MessageTypeRegistry
     */
    public MessageTypeRegistry() {
        this.descriptors = new HashSet<>();
        this.typeRegistry = null;
    }

    /**
     * @requires descriptor != null
     * @modifies this
     * @effects Registers the type described by the given descriptor in this
     */
    public void register(Descriptor descriptor) {
        Assert.notNull(descriptor);

        synchronized (this) {
            descriptors.add(descriptor);
            typeRegistry = null;
        }
    }

    /**
     * @return the registered types (as JsonFormat.TypeRegistry)
     */
    public JsonFormat.TypeRegistry typeRegistry() {
        synchronized (this) {
            if (typeRegistry == null) {
                typeRegistry =
                        JsonFormat.TypeRegistry.newBuilder()
                                .add(descriptors)
                                .build();
            }
            return typeRegistry;
        }
    }
}
