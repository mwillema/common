package com.marcowillemart.common.util;

import com.google.protobuf.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * Timestamps is a utility class that provides useful methods for manipulating
 * {@link com.google.protobuf.Timestamp} instances.
 *
 * @author Marco Willemart
 */
public final class Timestamps {

    private Timestamps() {
        Assert.shouldNeverGetHere();
    }

    /**
     * @requires localDateTime != null
     * @return localDateTime as Timestamp
     */
    public static Timestamp toTimeStamp(LocalDateTime localDateTime) {
        Assert.notNull(localDateTime);

        Instant instant = localDateTime.toInstant(ZoneOffset.UTC);

        return Timestamp.newBuilder()
                .setSeconds(instant.getEpochSecond())
                .setNanos(instant.getNano())
                .build();
    }

    /**
     * @requires localDate != null
     * @return localDate as Timestamp
     */
    public static Timestamp toTimeStamp(LocalDate localDate) {
        Assert.notNull(localDate);

        return toTimeStamp(localDate.atStartOfDay());
    }

    /**
     * @requires timestamp != null
     * @return timestamp as LocalDateTime
     */
    public static LocalDateTime toLocalDateTime(Timestamp timestamp) {
        Assert.notNull(timestamp);

        return LocalDateTime.ofEpochSecond(
                timestamp.getSeconds(),
                timestamp.getNanos(),
                ZoneOffset.UTC);
    }

    /**
     * @requires timestamp != null
     * @return timestamp as LocalDate
     */
    public static LocalDate toLocalDate(Timestamp timestamp) {
        return toLocalDateTime(timestamp).toLocalDate();
    }
}
