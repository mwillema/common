package com.marcowillemart.common.util;

import com.google.protobuf.Message;
import java.sql.Blob;

/**
 * MessageSerializer provides utility methods to serialize/deserialize Messages.
 *
 * @author Marco Willemart
 */
public final class MessageSerializer {

    /** The public static parseFrom(byte[]) method from Message. */
    private static final String PARSE_FROM = "parseFrom";

    /** this cannot be instantiated */
    private MessageSerializer() {
        throw new AssertionError();
    }

    /**
     * @requires message != null
     * @return a serialization <t,b> of 'message' where t is the type of message
     *         and b its serialized body.
     */
    public static Pair<String, byte[]> serialize(Message message) {
        return Pair.of(
                message.getClass().getName(),
                message.toByteArray());
    }

    /**
     * @requires type != null && body != null &&
     *           body is a serialized Message of the given type
     * @return 'body' as a Message of type T, T being the type named 'type'
     */
    public static <T extends Message> T deserialize(String type, Blob body) {
        return deserialize(type, IOUtils.toByteArray(body));
    }

    /**
     * @requires type != null && body != null &&
     *           body is a serialized Message of the given type
     * @return 'body' as a Message of type T, T being the type named 'type'
     */
    @SuppressWarnings("unchecked")
    public static <T extends Message> T deserialize(String type, byte[] body) {
        try {
            return (T) Class.forName(type)
                    .getMethod(PARSE_FROM, byte[].class)
                    .invoke(null, new Object[] {body});
        } catch (ReflectiveOperationException ex) {
            throw new FailureException("MessageSerializer.deserialize", ex);
        }
    }
}
