package com.marcowillemart.common.validation;

import com.marcowillemart.common.util.Assert;

/**
 * Abstract implementation of the TypeValidator interface.
 *
 * @author Marco Willemart
 */
public abstract class AbstractTypeValidator implements TypeValidator {

    private final Class<?> supportedType;

    /**
     * @requires supportType != null
     * @effects Makes this be a new validator for the given supported type of
     *          objects.
     */
    protected AbstractTypeValidator(Class<?> supportedType) {
        Assert.notNull(supportedType);

        this.supportedType = supportedType;
    }

    @Override
    public final Class<?> supportedType() {
        return supportedType;
    }

    @Override
    public final boolean supports(Class<?> clazz) {
        return supportedType.isAssignableFrom(clazz);
    }
}
