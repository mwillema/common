package com.marcowillemart.common.validation;

import org.springframework.validation.Validator;

/**
 * TypeValidator is a validator for a specific type of objects.
 *
 * @author Marco Willemart
 */
public interface TypeValidator extends Validator {

    /**
     * @return the supported type whose instances that can be validated by this
     */
    Class<?> supportedType();
}
