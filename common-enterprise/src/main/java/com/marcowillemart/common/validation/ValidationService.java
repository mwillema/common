package com.marcowillemart.common.validation;

import com.marcowillemart.common.util.Assert;
import com.marcowillemart.common.util.Lists;
import com.marcowillemart.common.util.NotPossibleException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;

/**
 * ValidationService is a stateless service responsible for validating specific
 * objects.
 *
 * @author Marco Willemart
 */
public class ValidationService {

    /** Error code when a target object is null. */
    public static final String TARGET_NULL_CODE = "target.null";

    /** Error message when a target object is null. */
    public static final String TARGET_NULL_MESSAGE =
            "Target object must not be null";

    private static final Logger LOG =
            LoggerFactory.getLogger(ValidationService.class);

    private final Map<Class<?>, Validator> typeTable;

    /**
     * @requires validators != null
     * @effects Makes this be a new validation service with the given
     *          validators.
     */
    public ValidationService(Set<TypeValidator> validators) {
        Assert.notNull(validators);

        this.typeTable = new HashMap<>(validators.size());

        for (TypeValidator validator : validators) {
            this.typeTable.put(validator.supportedType(), validator);
        }
    }

    /**
     * @throws NotPossibleException if 'target' is null or its validation fails.
     */
    public void validate(Object target) throws NotPossibleException {
        if (target == null) {
            LOG.warn(TARGET_NULL_MESSAGE);

            throw new NotPossibleException(
                    TARGET_NULL_MESSAGE,
                    TARGET_NULL_CODE);
        }

        if (typeTable.containsKey(target.getClass())) {
            DataBinder binder = new DataBinder(target);
            binder.setValidator(typeTable.get(target.getClass()));
            binder.validate();

            Errors errors = binder.getBindingResult();

            if (errors.hasErrors()) {
                ObjectError error = Lists.first(errors.getAllErrors());

                String code = error.getCode();
                Object[] arguments = argumentsFrom(error);
                String message = messageFrom(error, arguments);

                LOG.warn(message);

                throw new NotPossibleException(message, code, arguments);
            }
        } else {
            LOG.warn("Validator not found for {}",
                    target.getClass().getSimpleName());
        }
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    /**
     * @requires error != null
     * @return an array of arguments from the given error
     */
    private static Object[] argumentsFrom(ObjectError error) {
        if (error.getArguments() == null) {
            return new Object[0];
        }

        return error.getArguments();
    }

    /**
     * @requires error != null && arguments != null
     * @return a message from the given error with the arguments, if any.
     */
    private static String messageFrom(ObjectError error, Object[] arguments) {
        if (error.getDefaultMessage() == null) {
            return error.getCode();
        }

        return String.format(error.getDefaultMessage(), arguments);
    }
}
