package com.marcowillemart.common.util;

import com.google.protobuf.Any;
import com.marcowillemart.common.test.Event.DummyCreated;
import static com.marcowillemart.common.test.Tests.*;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests for the Messages class.
 *
 * @author Marco Willemart
 */
public class MessagesTest {

    @Test
    public void testTypeNameOf() {
        assertEquals(
                "module.MyMessage",
                Messages.typeNameOf("com.marcowillemart/module.MyMessage"));
        assertEquals(
                "module.MyMessage",
                Messages.typeNameOf("com.marcowillemart/common/module.MyMessage"));
    }

    @Test
    public void testClassForName() {
        // Exercise
        Class<DummyCreated> actual =
                Messages.classForName(
                        "com.marcowillemart.common",
                        "test.Event$DummyCreated");

        // Verify
        assertEquals(DummyCreated.class, actual);
    }

    @Test
    public void testUnpack() {
        // Setup
        DummyCreated event =
                DummyCreated.newBuilder()
                        .setId(createUUID())
                        .build();
        Any any = Any.pack(event);

        // Exercise & Verify
        assertEquals(event, Messages.unpack(any, DummyCreated.class));
    }
}
