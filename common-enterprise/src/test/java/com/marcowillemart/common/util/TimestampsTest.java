package com.marcowillemart.common.util;

import com.google.protobuf.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests for the Timestamps class.
 * @author Marco Willemart
 */
public class TimestampsTest {

    @Test
    public void testToTimeStamp_LocalDateTime() {
        // Setup
        LocalDateTime expected = LocalDateTime.now();
        Timestamp timestamp = Timestamps.toTimeStamp(expected);

        // Exercise & Verify
        assertEquals(expected, Timestamps.toLocalDateTime(timestamp));
    }

    @Test
    public void testToTimeStamp_localDate() {
        // Setup
        LocalDate expected = LocalDate.now();
        Timestamp timestamp = Timestamps.toTimeStamp(expected);

        // Exercise & Verify
        assertEquals(expected, Timestamps.toLocalDate(timestamp));
    }
}
