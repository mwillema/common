package com.marcowillemart.common.util;

import com.google.protobuf.Message;
import static com.marcowillemart.common.test.Tests.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for the MessageSerializer class.
 *
 * @author Marco Willemart
 */
public class MessageSerializerTest {

    private Message message;

    @Before
    public void setUp() {
        message = createAnonymousMessage();
    }

    @Test
    public void testSerialize() {
        // Exercise
        Pair<String, byte[]> serialization =
                MessageSerializer.serialize(message);

        // Verify
        assertEquals(message,
                MessageSerializer.deserialize(
                        serialization.left(),
                        serialization.right()));
    }
}
