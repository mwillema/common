package com.marcowillemart.common.util;

import com.google.protobuf.Descriptors.Descriptor;
import com.google.protobuf.util.JsonFormat.TypeRegistry;
import com.marcowillemart.common.test.Event.DummyCreated;
import com.marcowillemart.common.test.Event.DummyUpdated;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for the MessageTypeRegistry class.
 *
 * @author Marco Willemart
 */
public class MessageTypeRegistryTest {

    private static final Descriptor DESCRIPTOR_1 = DummyCreated.getDescriptor();
    private static final Descriptor DESCRIPTOR_2 = DummyUpdated.getDescriptor();

    private MessageTypeRegistry target;

    @Before
    public void setUp() {
        target = new MessageTypeRegistry();
    }

    @Test
    public void testRegister() {
        // Exercise
        target.register(DESCRIPTOR_1);

        // Verify
        TypeRegistry actual = target.typeRegistry();
        assertEquals(DESCRIPTOR_1, actual.find(DESCRIPTOR_1.getFullName()));
    }

    @Test
    public void testTypeRegistry_empty() {
        // Exercise
        TypeRegistry actual = target.typeRegistry();

        // Verify
        assertNull(actual.find(DESCRIPTOR_1.getFullName()));
        assertNull(actual.find(DESCRIPTOR_2.getFullName()));
    }

    @Test
    public void testTypeRegistry_same() {
        // Setup
        target.register(DESCRIPTOR_1);
        TypeRegistry expected = target.typeRegistry();

        // Exercise
        TypeRegistry actual = target.typeRegistry();

        // Verify
        assertSame(expected, actual);
    }

    @Test
    public void testTypeRegistry_different() {
        // Setup
        target.register(DESCRIPTOR_1);
        TypeRegistry unexpected = target.typeRegistry();
        target.register(DESCRIPTOR_2);

        // Exercise
        TypeRegistry actual = target.typeRegistry();

        // Verify
        assertNotSame(unexpected, actual);
    }
}
