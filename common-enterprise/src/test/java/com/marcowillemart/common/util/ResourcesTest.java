package com.marcowillemart.common.util;

import static com.marcowillemart.common.test.Tests.*;
import java.io.IOException;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.core.io.Resource;

/**
 * Unit tests for the Resources class.
 *
 * @author Marco Willemart
 */
public class ResourcesTest {

    @Test
    public void testFrom() throws IOException {
        // Setup
        final String expected = createUUID();

        // Exercise
        Resource resource = Resources.from(expected);

        // Verify
        String actual = IOUtils.toString(resource.getInputStream());
        assertEquals(expected, actual);
    }
}
