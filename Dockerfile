FROM maven:3.5.3-jdk-8 as BUILD
COPY . /app
COPY pom.xml /app
RUN mvn -f /app clean install -Pintegration-tests
